# PDFpower.exe

## Metadata

Filename: PDFpower.exe (SHA256: e248b01e3ccde76b4d8e8077d4fcb4d0b70e5200bf4e738b45a0bd28fbc2cae6)

Classification: PUA:Win32/MediaArena

Sample obtained from Malware Bazaar: https://bazaar.abuse.ch/sample/e248b01e3ccde76b4d8e8077d4fcb4d0b70e5200bf4e738b45a0bd28fbc2cae6

Decompiled with avaloniailspy 7.2 RC (C# 8.0 / VS 2019)

Part of the MediaArena suite of adware/browser-hijackers described in [this analysis by Northwave Cyber Security](https://northwave-cybersecurity.com/threat-intel-research/analysis-of-new-active-malware-mediaarena-pua).

## General Assessment

This program seems to mostly be concerned with hi-jacking the browser's default search page. The data transmitted from the application does not seem to be particularly sensitive (see below), neither does it seem to be performing any kind of keylogging or other info-stealing.

## Distribution & Installation

The executable seems to be dropped via malicious ads doing drive-by-downloading. Unless our analysis is completely missing a step, execution seems to be relying on users actively executing the program from their downloads folder.

## Information Gathering

 * OS version
   * Win XP up to (and including) Win 10
   * No 11?
 * Browser info
 * SUID, unique ID based on:
   * CPUID (derived from executing assembly/machine code)
   * Hard drive serial no.
   * Last 2 bytes of MAC address
     * Not working as intended because [UuidCreateSequential()](https://learn.microsoft.com/en-us/windows/win32/api/rpcdce/nf-rpcdce-uuidcreatesequential) no longer uses the machine's MAC address

## Tracking/Logging/Reporting

Some domains/URLs are hardcoded into the application whereas others (like: searchpoweronline[.].com) are easier to replace, so if you're threathunting for this, it may be worth your while to only search for the path part of some URLs, like "/v4/monetize" or "/v4/install/first_time", which is more likely to be consistent across strains.

 * Google Analytics
   * For tracking interaction with the application?
 * PdfPower.Struct.getIntServ
   * Calls: https://start.searchpoweronline[.]com/nav?string_interpolation=GET_OSOU&appId={{AppId}}&emid={{SUID}}
 * Desktop shortcut activated:
   * Calls: https://use.pdfconverterpower[.]net/
 * PdfPower.MainWindow.MainWindow_Loaded
   * Calls: searchpoweronline[.]com/v4/install/first_time
 * On InstallThreadCb:
   * If "objWithSearch", calls: https://www.pdfconverterpower[.]net/thankyou?tyid={{SUID}}
   * Else, calls: bl.searchpoweronline[.]com/v4/monetize
 * PdfPower.InstallationScreen.Control_Loaded
   * bl.searchpoweronline[.]com/v4/install
 * PdfPower.Simulation.simulationThreadCb
   * bl.searchpoweronline[.]com/v4/monetize
 * PdfPower.Struct.SendRepcRep
   * info.searchpoweronline[.]com/report

The above endpoints often receive encrypted data (see below) in one or more GET parameters.

Furthermore the program logs a lot of its steps via [System.Diagnostics.Trace](https://learn.microsoft.com/en-us/dotnet/api/system.diagnostics.trace) which makes it relatively easy to [follow the program's execution flow](https://app.any.run/tasks/aebc6abe-ef03-4e5a-a37f-3d7c19d267c4/).

## Encoding/encryption

 * Simple XOR + base64
 * Keys:
   * B_INF_KEY = 'anabel' (rarely used, only for some browser version info inside data already encrypted with the below key)
   * JSON_key = 'takton' (used for most communication)

Decryption can be done with this [CyberChef recipe](https://gchq.github.io/CyberChef/#recipe=URL_Decode()From_Base64(%27A-Za-z0-9%2B/%3D%27,true,false)XOR(%7B%27option%27:%27UTF8%27,%27string%27:%27takton%27%7D,%27Standard%27,false)JSON_Beautify(%27%20%20%20%20%27,false,true)) by Hardet.

## Persistence

There doesn't seem to be much of anything in the way of persistence.

## Uninstall

PdfPower.Struct.AddUnistallToRegistry

 * Registry keys for removal
 * Deletes desktop shortcut and a temp folder created during install/initialization
 * Calls specific URLs:
   * www.pdfconverterpower[.]com/goodbye
   * https://www.searchpoweronline[.]com/bye
 * Does not restore browser settings!

## Todo:

 * Map general program flow
 * Persistence?
 * Callbacks (e.g. GetInterpolationValue())
